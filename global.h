#ifndef GLOBALTYPE_H
#define GLOBALTYPE_H
#include <QString>
#include <QVariantMap>
#include <QVariantList>
#include <QList>
#include <QSqlRecord>
#include <functional>
#include "route/route.h"
#include "lib/CConfig/cconfig.h"
#include "lib/CSqlOperator/csqloperator.h"
#include "lib/CErrorDebug/cerrordebug.h"
#include "lib/CEpsonPrinter/cepsonprinter.h"
#include "lib/CTscPrinter/ctscprint.h"

#include "mealstruct.h"
#include "checkoutstruct.h"
#include "discountstruct.h"
#include "mealinfo.h"
#include "specialstruct.h"
#include "typestruct.h"
#include "billsstruct.h"


#ifdef QT_DEBUG
#define DB_PATH "D:\\projectProgram\\BitBucket\\PosSystem\\PosSystem.db"
#define CONFIG_PATH "D:\\projectProgram\\BitBucket\\PosSystem\\PosSystem.config"
#define TSCDLL_PATH "D:\\TSCLIB.dll"
#define NUMBER_PATH "D:\\projectProgram\\BitBucket\\PosSystem\\number"
#else
#define DB_PATH "..\\store\\PosSystem.db"
#define CONFIG_PATH "..\\store\\PosSystem.config"
#define TSCDLL_PATH "TSCLIB.dll"
#define NUMBER_PATH "..\\store\\number"
#endif

extern void initAll();


extern Route* g_route;
extern CConfig g_config;
extern CSqlOperator g_db;
extern QList<MealStruct> g_mealList;
extern QList<TypeStruct> g_typeList;
extern CEpsonPrinter g_epsonPrinter;
extern CTscPrinter g_tscPrinter;
extern BillsStruct g_billStruct;
extern int g_number;

#endif // GLOBALTYPE_H
