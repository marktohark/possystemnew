#ifndef MEALSTRUCT_H
#define MEALSTRUCT_H
#include <QVariantMap>
#include <functional>
#include "specialstruct.h"
#include "mealinfo.h"
#include "discountstruct.h"

class MealStruct {
public:
    MealStruct(int _id = -1, QString _name = "", int __class = -1, int _typeId = -1,int _count = 1);

    MealStruct(QVariantMap data);

    QVariantMap toMap();

    void byMap(QVariantMap data);

    void addSpecialListForeach(std::function<void(SpecialStruct&)> func);

    void subSpecialListForeach(std::function<void(SpecialStruct&)> func);

    void infoListForeach(std::function<void(MealInfo&)> func);

    int getTotalPrice();//總價 含折扣

    int getSinglePrice(); //單價

    MealStruct copyClearObj();
public:
    enum CLASS_ENUM { // 吃或喝
        enum_non = -1,
        enum_eat,
        enum_drink,
        enum_end
    };
    int id{0};
    QString name{""};
    CLASS_ENUM _class{enum_non};
    int typeId{-1};
    int count{1};
    QList<MealInfo> infoList;
    SpecialStruct sugar; //糖
    SpecialStruct ice; //冰塊
    QList<SpecialStruct> addSpecialList; //加肉 加但
    QList<SpecialStruct> subSpecialList; //不要肉 不要但
    DiscountStruct discount;
};

#endif // MEALSTRUCT_H
