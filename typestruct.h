#ifndef TYPESTRUCT_H
#define TYPESTRUCT_H
#include <QVariantMap>

class TypeStruct {
public:
    TypeStruct(int _id = -1, QString _name = "");
    TypeStruct(QVariantMap data);
    QVariantMap toMap();
    void byMap(QVariantMap data);
public:
    int id{-1};
    QString name{""};
};

#endif // TYPESTRUCT_H
