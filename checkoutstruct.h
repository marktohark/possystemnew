#ifndef CHECKOUTSTRUCT_H
#define CHECKOUTSTRUCT_H
#include <QVariantMap>
#include <functional>
#include "mealstruct.h"

class CheckOutStruct {
public:
    enum ENUM_TRANSFER {
        enum_non = -1,
        enum_takeOut,//外帶
        enum_eatHere,//內用
        enum_takeSelf,//自取
        enum_order,//外送
        enum_end
    };

    CheckOutStruct(int _number = 0,
                   int _payMoney = 0,
                   int _totalPrice = 0,
                   ENUM_TRANSFER _takeOut = enum_eatHere,
                   QList<MealStruct> _mealList = QList<MealStruct>());

    QString getCurTransferStr();//enum 2 str

    void mealLiistForeach(std::function<void(MealStruct&)> func);

    void clear();

    bool isEmpty() {return mealList.isEmpty();}

    void calcMealTotalPrice();
public:
    int number{0};
    int payMoney{0};
    int totalPrice{0};
    ENUM_TRANSFER takeOut{enum_eatHere};
    QList<MealStruct> mealList;
};

#endif // CHECKOUTSTRUCT_H
