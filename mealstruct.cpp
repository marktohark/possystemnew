#include "mealstruct.h"

MealStruct::MealStruct(int _id, QString _name, int __class, int _typeId,int _count)
    :id(_id), name(_name), _class((CLASS_ENUM)__class), typeId(_typeId),count(_count) {

}

MealStruct::MealStruct(QVariantMap data) {
    byMap(data);
}

QVariantMap MealStruct::toMap() {
    //{id:??,name:??,class:??,typdId:??,count:???,infoList: [MealInfo.toMap],addSpecialList:[SpecialStruct.toMap],subSpecialList}
    QVariantMap result;
    QVariantList _infoList;
    QVariantList add_specialList;
    QVariantList sub_specialList;
    result["id"] = id;
    result["name"] = name;
    result["class"] = (int)_class;
    result["typeId"] = typeId;
    result["count"] = count;
    infoListForeach([&](MealInfo& item){
        _infoList.append(item.toMap());
    });
    addSpecialListForeach([&](SpecialStruct& item){
        add_specialList.append(item.toMap());
    });
    subSpecialListForeach([&](SpecialStruct& item){
        sub_specialList.append(item.toMap());
    });
    result["infoList"] = _infoList;
    result["addSpecialList"] = add_specialList;
    result["subSpecialList"] = sub_specialList;
    if(!sugar.isEmpty)
        result["sugar"] = this->sugar.toMap();
    if(!ice.isEmpty)
        result["ice"] = this->ice.toMap();
    return result;
}

void MealStruct::byMap(QVariantMap data) {
    auto iter = data.find("id");
    if(iter != data.end())
        id = (*iter).toInt();

    iter = data.find("name");
    if(iter != data.end())
        name = (*iter).toString();

    iter = data.find("class");
    if(iter != data.end())
        _class = (CLASS_ENUM)(*iter).toInt();

    iter = data.find("typeId");
    if(iter != data.end())
        typeId = (*iter).toInt();

    iter = data.find("count");
    if(iter != data.end())
        count = (*iter).toInt();

    iter = data.find("infoList");
    if(iter != data.end()) {
        QVariantList _infoList = (*iter).toList();
        for(const QVariant& item : _infoList)
            infoList.append(MealInfo(item.toMap()));
    }

    iter = data.find("addSpecialList");
    if(iter != data.end()) {
        QVariantList add_specialList = (*iter).toList();
        for(const QVariant& item : add_specialList)
            addSpecialList.append(SpecialStruct(item.toMap()));
    }

    iter = data.find("subSpecialList");
    if(iter != data.end()) {
        QVariantList sub_specialList = (*iter).toList();
        for(const QVariant& item : sub_specialList)
            subSpecialList.append(SpecialStruct(item.toMap()));
    }
}

void MealStruct::addSpecialListForeach(std::function<void(SpecialStruct&)> func) {
    for(SpecialStruct& item : addSpecialList)
        func(item);
}

void MealStruct::subSpecialListForeach(std::function<void(SpecialStruct&)> func) {
    for(SpecialStruct& item : subSpecialList)
        func(item);
}

void MealStruct::infoListForeach(std::function<void(MealInfo&)> func) {
    for(MealInfo& item : infoList)
        func(item);
}

int MealStruct::getTotalPrice() {
    return discount.getPrice(*this);
}

int MealStruct::getSinglePrice() {
    return infoList.at(0).price;
}

MealStruct MealStruct::copyClearObj() {
    MealStruct temp(
                this->id,
                this->name,
                this->_class,
                this->typeId,
                this->count
                );
    temp.infoList.append(this->infoList.at(0));
    temp.sugar = this->sugar;
    temp.ice = this->ice;
    temp.discount = this->discount;
    return temp;
}
