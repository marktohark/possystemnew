#ifndef SPECIALSTRUCT_H
#define SPECIALSTRUCT_H

#include <QVariantMap>

class SpecialStruct {
public:
    enum ENUM_MODE {
        enum_non = -1,
        enum_add,
        enum_sub,
        enum_end
    };

    SpecialStruct(int _id = -1, QString _name = "", int _price = -1, ENUM_MODE _mode = enum_non, bool _isEmpty = true);

    SpecialStruct(QVariantMap data);

    QVariantMap toMap(); //{id:??,name:??,price:??}

    void byMap(QVariantMap data);
public:
    int id{-1};
    QString name{""};
    int price{-1};
    ENUM_MODE mode{enum_non};
    bool isEmpty{true};
};

#endif // SPECIALSTRUCT_H
