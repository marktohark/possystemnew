import QtQuick 2.0
import "qrc:///view/Control"

MyPanel {
    id: root
    border.width: 1

    Column {
        //數字按鈕
        Repeater {
            model: [['1','2','3'],['4','5','6'],['7','8','9'],['00','0']]

            Row {
                Repeater {
                    model: modelData

                    MyButton {
                        width: root.width / 3
                        height: root.height / 5
                        text: modelData
                        onClicked: Route.post('KeyBoardController@c_sendButton',{command: modelData})
                    }
                }
            }
        }

        //清除按鈕
        MyButton {
            width: root.width
            height: root.height / 5
            text: '清除'
            onClicked: Route.post('KeyBoardController@c_sendButton',{command: 'clr'})
        }
    }
}
