import QtQuick 2.0
import "qrc:///view/Control"

Rectangle {
    signal close()
    default property alias content: _panel.data

    id: root
    color: Qt.rgba(1,1,1,0.3)
    visible: false
    Rectangle {
        id: _panel
        color: Qt.rgba(0,0,0,1)
        border {
            width: 1
            color: "green"
        }
        anchors.right: parent.right
        width: parent.width * 2 / 3
        height: parent.height
    }

    MouseArea {
        id:_mouseArea
        anchors.left: parent.left
        width: parent.width / 3
        height: parent.height
        onClicked: {
            root.close()
            root.visible = false
        }
    }
}
