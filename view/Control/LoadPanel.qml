import QtQuick 2.0

Rectangle {
    color: Qt.rgba(0,0,0,0)
    border {
        width: 5
        color: "#22FF64"
    }
    anchors {
        fill: parent
        margins: 50
    }
    TextRWD {
        id:_text
        anchors {
            centerIn: parent
            fill:parent
            margins: 50
        }
    }
    Connections {
        target: LoadPanelController
        onSendText: _text.text = text
    }
    Component.onCompleted: {LoadPanelController.initGlobal();console.log(123)}
}
