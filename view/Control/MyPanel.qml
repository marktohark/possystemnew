import QtQuick 2.0

Rectangle {
    id: root
    color: Qt.rgba(0,0,0,1)
    radius: 10
    border {
        width: 5
        color: "#22FF64"
    }
}
