import QtQuick 2.0

MyPanel {
    property alias text: _text.text
    property var checked: false
    signal clicked()

    id:root

    MouseArea {
        id:_mouse
        anchors.fill: parent
        onClicked: root.clicked(mouse)
    }
    TextRWD {
        id: _text
        anchors.fill: parent
        color: "#22FF64"
    }
    states: [
        State {
            name: "checked"
            when: root.checked
            PropertyChanges {
                target: root
                color:Qt.rgba(0,0,1,0.7)
            }
        }
    ]

    function setCheckBox(val) {
        root.checked = val
    }

    function toggleCheckBox() {
        root.checked = !root.checked
    }

    function isChecked() {
        return root.checked
    }
}
