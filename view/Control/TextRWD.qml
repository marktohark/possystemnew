import QtQuick 2.0

Text {
    color:"#22FF64"
    verticalAlignment: Text.AlignVCenter
    horizontalAlignment: Text.AlignHCenter
    font.pointSize: 100
    minimumPointSize: 10
    fontSizeMode: Text.Fit
    wrapMode:Text.Wrap
    font.bold: true
}
