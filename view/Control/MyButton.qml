import QtQuick 2.0

MyPanel {
    property alias text: _text.text
    signal clicked()
    id:root

    MouseArea {
        id:_mouse
        anchors.fill: parent
        onClicked: root.clicked(mouse)
    }
    TextRWD {
        id: _text
        anchors {
            fill: parent
            margins: 5
        }
        color: "#22FF64"
    }
    states: [
        State {
            name: "pressed"
            when:_mouse.pressed
            PropertyChanges {
                target: root
                color:Qt.rgba(0,0,1,0.7)
            }
        }
    ]
}
