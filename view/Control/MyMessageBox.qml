import QtQuick 2.7
import QtQuick.Window 2.2

Rectangle {
    id:root
    width: Screen.width
    height: Screen.height
    color: Qt.rgba(0,0,0,0.3)
    visible: false

    Rectangle {
        id: _msgPanel
        color: Qt.rgba(0,0,0,1)
        width: parent.width / 2
        height: parent.height / 2
        anchors.centerIn: parent
        border {
            width: 5
            color: "#22FF64"
        }

        TextRWD {
            id:_text
            width: parent.width
            height: parent.height * 2 / 3
            anchors {
                top:parent.top
                margins: 5
            }
        }

        Row {
            spacing: 10
            leftPadding: 20
            width: parent.width
            height: parent.height / 3
            anchors {
                left: parent.left
                right: parent.right
                top: _text.bottom
                bottom: parent.bottom
                bottomMargin: 10
            }

            MyButton {
                id:_yesBtn
                text: "是"
                width: (parent.width - parent.spacing - parent.leftPadding * 2) / 2
                height: parent.height
                onClicked: Route.post("MyMsgBoxController@c_emitFinish",{
                                          btn: true
                                      })
            }

            MyButton {
                id:_noBtn
                text: "否"
                width: _yesBtn.width
                height: parent.height
                onClicked: Route.post("MyMsgBoxController@c_emitFinish",{
                                          btn: false
                                      })
            }
        }
    }

    Connections {
        target: MyMsgBoxController
        onShow: {
            _text.text = data.text
            root.visible = true
        }

        onClose: root.visible = false
    }
}
