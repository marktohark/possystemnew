import QtQuick 2.0
import "qrc:///view/Control"

MyPanel {
    id: root
    border.width: 1

    Column {
        anchors.bottom: parent.bottom
        MyButton {
            id: _addBag
            width: root.width
            height: root.height / 5
            text: "加購袋子"
            onClicked: Route.post('FunctionalButtonController@c_addBag')
        }

        MyButton {
            id: _openBillList
            width: root.width
            height: root.height / 5
            text: "清單"
            onClicked: Route.post('SelectBillController@c_show')
        }

        MyButton {
            id: _openDrawer
            width: root.width
            height: root.height / 5
            text: "錢櫃"
            onClicked: Route.post('FunctionalButtonController@c_openDrawer')
        }


        MyButton {
            id: _deleteCheckOut
            width: root.width
            height: root.height / 5
            text: "刪單"
            onClicked: Route.post('CheckOutListController@c_deleteCheckOut')
        }

        MyButton {
            id: _account
            width: root.width
            height: root.height / 5
            text: "結帳"
            onClicked: Route.post("FunctionalButtonController@c_print")
        }
    }
}
