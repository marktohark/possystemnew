import QtQuick 2.0
import "qrc:///view/Control"

MyPanel {
    id: root
    border.width: 1

    TextRWD {
        id: _number
        width: root.width
        height: root.height / 6
        horizontalAlignment: Text.AlignLeft
        anchors {
            top:root.top
        }

        text: "號碼："
    }

    TextRWD {
        id: _payPrice
        width: root.width
        height: root.height / 6
        horizontalAlignment: Text.AlignLeft
        anchors {
            top:_number.bottom
        }
        text:"付款："
    }

    TextRWD {
        id: _price
        width: root.width
        height: root.height / 6
        horizontalAlignment: Text.AlignLeft
        anchors {
            top:_payPrice.bottom
        }
        text: "價格:"
    }

    TextRWD {
        id: _changed //找錢
        width: root.width
        height: root.height / 6
        horizontalAlignment: Text.AlignLeft
        anchors {
            top:_price.bottom
        }
        text: "找錢:"
    }


    Column {
        id: _column
        width: root.width
        height: root.height * 2 / 6
        anchors {
            top:_changed.bottom
        }

        Repeater {
            model: [['內用','外帶'],['外送','自取']]
            Row {
                Repeater {
                    model: modelData

                    MyCheckBox {
                        id: _checkBox
                        width: root.width / 2
                        height: root.height / 6
                        text:modelData
                        onClicked: {
                            Route.post('DetailTableController@c_setTransfer',{typeStr: modelData})
                            Route.post('DetailTableController@c_clearCheckBox')
                            _checkBox.checked = true;
                        }
                        Connections {
                            target: DetailTableController
                            onClearCheckBox: _checkBox.checked = false
                            onUpdateTransfer: _checkBox.checked = (modelData === transferStr)
                        }
                    }
                }
            }
        }
    }

    Connections {
        target: DetailTableController
        onUpdatePayMoney: _payPrice.text = "付款：" + payMoney
        onUpdateNumber: _number.text = "號碼：" + number
        onUpdateTotalMoney: _price.text = "價格：" + totalMoney
        onUpdateChange: _changed.text = "找錢：" + change
    }
}
