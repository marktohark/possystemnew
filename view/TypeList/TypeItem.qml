import QtQuick 2.0
import "qrc:///view/Control"
Item {
    id:root
    MyButton {
        id:_button
        text:typeName
        anchors {
            fill: parent
            margins: 5
        }
        onClicked: Route.post('TypeListController@c_emitTypeId',{typeId:typeId})
    }
}
