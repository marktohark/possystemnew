import QtQuick 2.0

Rectangle {
    id: root
    color: Qt.rgba(0,0,0,0)
    border {
        width: 1
        color: "green"
    }

    ListView {
        anchors.fill: parent
        model: TypeListController.typeListModel
        delegate: TypeItem {
            width: root.width
            height: root.width
        }
        Component.onCompleted: Route.post('TypeListController@c_updateList')
    }
}
