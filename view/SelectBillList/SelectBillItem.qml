import QtQuick 2.0
import "qrc:///view/Control"

Item {
    id: root
    MyCheckBox {
        id:checkBox
        text: modelData
        anchors {
            fill:parent
            margins: 5
        }
        onClicked: {
            Route.post('SelectBillController@c_setCurBillByIndex',{index:modelData})
            Route.post('SelectBillController@c_close')
        }
        Connections {
            target: SelectBillController
            onShow: {
                var isEmpty = Route.post("SelectBillController@c_isEmptyByIndex",{index: index})["isEmpty"]
                checkBox.checked = !isEmpty
            }
        }
    }
}
