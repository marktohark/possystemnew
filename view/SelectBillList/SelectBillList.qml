import QtQuick 2.0

Rectangle {
    id: root
    color: Qt.rgba(0,0,0,1)
    visible: true
    border {
        width: 1
        color: "green"
    }

    GridView {
        id: _gridview
        anchors.fill: parent
        model: SelectBillController.getBillListModel
        cellWidth: root.width / 5
        cellHeight: root.width / 5
        delegate: SelectBillItem {
            width: _gridview.cellWidth
            height: _gridview.cellHeight
        }
    }

    Connections {
        target: SelectBillController
        onShow: root.visible = true
        onClose: root.visible = false
    }
}
