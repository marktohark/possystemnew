import QtQuick 2.0
import "qrc:///view/Control"

Row {
    signal clicked(var curCheckBox,var modelData)
    signal unChecked(var curCheckBox)
    signal setCheckByVal(var val)
    property alias model: _checkBoxRepeater.model
    property var key: ''
    property var value: ''
    id: root
    width: parent.width
    height: parent.height / 5
    Repeater {
        id: _checkBoxRepeater
        MyCheckBox {
            id: _checkBox
            text: key === '' ? modelData : modelData[key]
            height: parent.height
            width: parent.width / 7
            onClicked: root.clicked(_checkBox,modelData)
        }
    }

    function setCheckedByValue(val) {
        for(var i in _checkBoxRepeater.model) {
            var checkBox = _checkBoxRepeater.itemAt(i)
            if(value === '')
                checkBox.checked = (_checkBoxRepeater.model[i] == val)
            else
                checkBox.checked = (_checkBoxRepeater.model[i][value] == val)
        }
    }

    function unCheckedAll() {
        for(var i in _checkBoxRepeater.model)
            _checkBoxRepeater.itemAt(i).checked = false
    }
}
