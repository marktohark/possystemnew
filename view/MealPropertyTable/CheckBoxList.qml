import QtQuick 2.0
import "qrc:///view/Control"
Row {
    signal clicked(var curCheckBox, var modelData)
    signal completed(var curCheckBox, var modelData)
    signal isExist(var curCheckBox, var modelData)
    property alias model: _checkList.model
    id: root

    width: parent.width
    height: parent.height / 5

    ListView {
        id: _checkList
        anchors.fill: parent
        clip: true
        orientation: ListView.Horizontal
        delegate: MyCheckBox {
            id: _checkBox
            text: modelData.name
            height: _checkList.height
            width: _checkList.width / 6
            onClicked: root.clicked(_checkBox, modelData)
            Component.onCompleted: root.completed(_checkBox, modelData)
            Connections {
                id:_checkBoxConnect
                target: MealPropertyTableController
                onSetCheckBoxByExist: root.isExist(_checkBox, modelData)
            }
        }
        cacheBuffer: 1
    }
}
