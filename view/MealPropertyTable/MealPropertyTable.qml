import QtQuick 2.0
import "qrc:///view/Control"

SidePanel {
    property bool isDrink: true
    id: root
    Column {
        anchors.fill: parent

        //sugar
        RadioControl {
            id: _sugarRadio
            visible: root.isDrink
            anchors.top: parent.top
            key: ''
            model:["正常","全糖","八分糖","半糖","微糖","無糖"]
            onClicked: {
                Route.post('MealPropertyTableController@c_setSugar',{sugar: modelData})
                curCheckBox.checked = true
                console.log(root.isDrink)
            }
            Connections {
                target: MealPropertyTableController
                onSugarUnChecked: _sugarRadio.unCheckedAll()
                onSugarCheckedByValue: _sugarRadio.setCheckedByValue(val)
            }
        }

        //ice
        RadioControl {
            id: _iceRadio
            visible: root.isDrink
            anchors.top: _sugarRadio.bottom
            key: ''
            model:["正常","多冰","少冰","微冰","去冰","溫", "熱"]
            onClicked: {
                Route.post('MealPropertyTableController@c_setIce',{ice: modelData})
                curCheckBox.checked = true
            }
            Connections {
                target: MealPropertyTableController
                onIceUnChecked: _iceRadio.unCheckedAll()
                onIceCheckByValue: _iceRadio.setCheckedByValue(val)
            }
        }

        //add
        CheckBoxList {
            id: _addSpecialList
            anchors.top: _iceRadio.bottom
            onClicked: {
                curCheckBox.checked = !curCheckBox.checked
                Route.post("MealPropertyTableController@c_setSpecial",{
                               specialId: modelData.id,
                               set: curCheckBox.checked,
                               mode: "add"
                           })
            }
            onIsExist: root.checkSpecialExist(curCheckBox,modelData,"add")
            onCompleted: root.checkSpecialExist(curCheckBox,modelData,"add")
        }

        //sub
        CheckBoxList {
            id: _subSpecialList
            anchors.top: _addSpecialList.bottom
            onClicked: {
                curCheckBox.checked = !curCheckBox.checked
                Route.post("MealPropertyTableController@c_setSpecial",{
                               specialId: modelData.id,
                               set: curCheckBox.checked,
                               mode: "sub"
                           })
            }
            onIsExist: root.checkSpecialExist(curCheckBox,modelData,"sub")
            onCompleted: root.checkSpecialExist(curCheckBox,modelData,"sub")
        }
        //size
        RadioControl {
            id: _sizeRadio
            anchors.top: _subSpecialList.bottom
            key: 'sizeStr'
            value: 'size'
            onClicked: {
                Route.post('MealPropertyTableController@c_setSize',{size: modelData.size})
                curCheckBox.checked = true
            }
            Connections {
                target: MealPropertyTableController
                onSizeUnChecked: _sizeRadio.unCheckedAll()
                onSizeCheckByValue: _sizeRadio.setCheckedByValue(val)
            }
        }
    }
    Connections {
        target: MealPropertyTableController
        onShow: root.visible = true
        onClose: root.visible = false
        onSetMealProperty: {
            root.isDrink = (mealProperty.class === 1)

            if(root.isDrink) {
                _sugarRadio.setCheckedByValue(mealProperty.sugar.name)
                _iceRadio.setCheckedByValue(mealProperty.ice.name)
            }
            _addSpecialList.model = Route.post("MealPropertyTableController@c_getAddSpecialModel")["addSpecialList"]
            _subSpecialList.model = Route.post("MealPropertyTableController@c_getSubSpecialModel")["subSpecialList"]
            _sizeRadio.model = Route.post('MealPropertyTableController@c_getSizeModel')["sizeModel"]
            Route.post("MealPropertyTableController@c_setCheckBoxByExist")
            _sizeRadio.setCheckedByValue(mealProperty.infoList[0].size)
        }
    }

    function checkSpecialExist(curCheckBox,modelData,mode) {
        var specialList = Route.post("MealPropertyTableController@c_getCurSpecialList",{mode:mode})["specialList"]
        for(var i in specialList)
            if(specialList[i].id === modelData.id) {
                curCheckBox.checked = true
                return
            }
        curCheckBox.checked = false
    }
}
