import QtQuick 2.0
import "qrc:///view/Control"

MyPanel {
    property var buttonCount: 7

    id: root
    border.width: 1
    radius: 0

    Column {
        anchors.fill: parent
        spacing: 5

        MyButton {
            id: _add
            width: parent.width
            height: parent.height / root.buttonCount
            anchors {
                top: parent.top
            }
            text: "+"
            onClicked: Route.post('CheckOutListController@c_addButton')
        }

        MyButton {
            id: _sub
            width: parent.width
            height: parent.height / root.buttonCount
            anchors {
                top: _add.bottom
            }
            text: "-"
            onClicked: Route.post('CheckOutListController@c_subButton')
        }

        MyButton {
            id: _free
            width: parent.width
            height: parent.height / root.buttonCount
            anchors {
                top: _sub.bottom
            }
            text: "贈"
            onClicked: Route.post('CheckOutListController@c_freeButton')
        }

        MyButton {
            id: _copy
            width: parent.width
            height: parent.height / root.buttonCount
            anchors {
                top: _free.bottom
            }
            text: "複製"
            onClicked: Route.post('CheckOutListController@c_copyButton')
        }

        MyButton {
            id: _mealProperty
            width: parent.width
            height: parent.height / root.buttonCount
            anchors {
                top: _copy.bottom
            }
            text: "詳細"
            onClicked: Route.post('MealPropertyTableController@c_show')
        }

        MyButton {
            id: _discount
            width: parent.width
            height: parent.height / root.buttonCount
            anchors {
                top: _mealProperty.bottom
            }
            text: "折價"
            onClicked: Route.post('DiscountTableController@c_show')
        }

        MyButton {
            id: _delete
            width: parent.width
            height: parent.height / root.buttonCount
            anchors {
                top: _discount.bottom
            }
            text: "刪除"
            onClicked: Route.post('CheckOutListController@c_deleteButton')
        }

    }
}
