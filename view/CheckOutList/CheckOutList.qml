import QtQuick 2.0
import "Board"

Rectangle {
    id: root
    color: Qt.rgba(0,0,0,1)
    border {
        width: 1
        color: "green"
    }

    ListView {
        id: _listView
        clip: true
        model: CheckOutListController.checkOutListModel
        delegate: CheckOutListItem {
            width: _listView.width
            height: _listView.height * 1.5 / 5
        }
        width: parent.width * 2 / 3
        height: parent.height

        highlightRangeMode: ListView.NoHighlightRange
        highlightMoveDuration : 200
        highlightMoveVelocity : 1000
        highlight: Component {
            Rectangle{
                opacity: 0.5
                color: "#0000FF"
                radius: 10
            }
        }

        Connections {
            target: CheckOutListController
            onSetSelectedIndex: _listView.currentIndex = selectedIndex
            onMoveLastItem: {
                _listView.currentIndex = _listView.count - 1
                Route.post('CheckOutListController@c_selectMeal',{index: _listView.currentIndex})
            }
            onMoveToItem: _listView.currentIndex = toIndex
            onGetCurScrollPos: CheckOutListController.Buffer = _listView.contentY
            onSetScrollToPos: _listView.contentY = pos
        }
    }

    Board {
        id: _board
        width: parent.width / 3
        height: parent.height
        anchors {
            left: _listView.right
            top: _listView.top
        }
    }
}
