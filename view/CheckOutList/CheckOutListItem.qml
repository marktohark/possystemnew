import QtQuick 2.0
import "qrc:///view/Control"

Item {
    id: root
    MyCheckBox {
        id: _panel
        color: Qt.rgba(0,0,0,0)
        anchors {
            fill:parent
            margins: 5
        }
        onClicked: Route.post('CheckOutListController@c_selectMeal',{index: index})

        Item {
            anchors {
                fill: parent
                margins: 10
            }
            //餐點名稱
            TextRWD {
                id: _mealName
                width: parent.width
                height: parent.height / 4
                anchors {
                    left: parent.left
                    top: parent.top
                }
                horizontalAlignment: Text.AlignLeft
                text: mealName
            }

            //數量
            TextRWD {
                id: _countTitle
                width: parent.width
                height: parent.height / 4
                anchors.top: _mealName.bottom
                horizontalAlignment: Text.AlignLeft
                text: "數量"
            }
            TextRWD {
                id: _countValue
                anchors {
                    left: _countTitle.left
                    right: _countTitle.right
                    top: _countTitle.top
                    bottom: _countTitle.bottom
                }
                horizontalAlignment: Text.AlignLeft
                LayoutMirroring.enabled: true
                text: count
            }

            //糖 冰 大小
            TextRWD {
                id: _sugarAndIceAndSize
                width: parent.width
                height: parent.height / 4
                anchors.top: _countTitle.bottom
                horizontalAlignment: Text.AlignLeft
                text: specialArray
            }

            //價格
            TextRWD {
                id: _priceTitle
                width: parent.width
                height: parent.height / 4
                anchors.top: _sugarAndIceAndSize.bottom
                horizontalAlignment: Text.AlignLeft
                text: "價錢"
            }
            TextRWD {
                id: _priceValue
                anchors {
                    left: _priceTitle.left
                    right: _priceTitle.right
                    top: _priceTitle.top
                    bottom: _priceTitle.bottom
                }
                horizontalAlignment: Text.AlignLeft
                LayoutMirroring.enabled: true
                text: totalPrice
            }
        }
    }
}


