import QtQuick 2.0

Rectangle {
    id: root
    color: Qt.rgba(0,0,0,0)
    border {
        width: 1
        color: "green"
    }

    GridView {
        id: _gridview
        clip: true
        anchors.fill: parent
        model: MealListController.mealListModel
        cellWidth: root.width / 4
        cellHeight: root.width / 4
        delegate: MealItem {
            width: _gridview.cellWidth
            height: _gridview.cellHeight
        }
    }
}
