import QtQuick 2.0
import "qrc:///view/Control"

Item {
    id: root
    MyButton {
        text: mealName
        anchors {
            fill:parent
            margins: 5
        }
        onClicked: Route.post('MealListController@addMealToCheckOut',{mealId:mealId})
    }
}
