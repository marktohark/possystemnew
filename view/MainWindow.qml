import QtQuick 2.0
import QtQuick.Layouts 1.3
import "qrc:///view/Control"
import "qrc:///view/TypeList"
import "qrc:///view/MealList"
import "qrc:///view/SelectBillList"
import "qrc:///view/CheckOutList"
import "qrc:///view/DetailTable"
import "qrc:///view/FunctionalButton"
import "qrc:///view/DiscountTable"
import "qrc:///view/MealPropertyTable"

Rectangle {
    anchors.fill: parent
    color: Qt.rgba(0,0,0,1)

    TypeList {
        id:_typeList
        width: parent.width / 7
        anchors {
            right: parent.right
            top: parent.top
            bottom: parent.bottom
        }
    }

    CheckOutList {
        id: _checkOutList
        width: parent.width / 2
        anchors {
            left: parent.left
            top:parent.top
            bottom: parent.verticalCenter
        }
    }

    MealList {
        id: _mealList
        anchors {
            left: _checkOutList.right
            right: _typeList.left
            top:parent.top
            bottom: _typeList.verticalCenter
        }
    }

    MyKeyBoard {
        id:_keyBoard
        width: _checkOutList.width * 2 / 3
        anchors {
            left: parent.left
            top: _checkOutList.bottom
            bottom: parent.bottom
        }
    }

    DetailTable {
        id: _detailTable
       width: _keyBoard.width
       anchors {
           left: _checkOutList.right
           right: _typeList.left
           top: _mealList.bottom
           bottom: parent.bottom
       }
    }

    FunctionalButton {

        anchors {
            left: _keyBoard.right
            right: _detailTable.left
            top: _detailTable.top
            bottom: parent.bottom
        }
    }

    SelectBillList {
        anchors.fill: parent
        z:2
    }

    DiscountTable {
        anchors.fill: parent
        z:2
    }

    MealPropertyTable {
        anchors.fill: parent
        z:2
    }
}
