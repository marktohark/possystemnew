import QtQuick 2.0
import "qrc:///view/Control"

SidePanel {
    id: root
    GridView {
        id: _gridview
        clip: true
        anchors.fill: parent
        model: [
            {value: -1, name: "無"},
            {value: 1, name: "1折"},
            {value: 2, name: "2折"},
            {value: 3, name: "3折"},
            {value: 4, name: "4折"},
            {value: 5, name: "5折"},
            {value: 6, name: "6折"},
            {value: 7, name: "7折"},
            {value: 8, name: "8折"},
            {value: 9, name: "9折"}
        ]
        cellWidth: parent.width / 4
        cellHeight: parent.width / 4
        delegate: DiscountTableItem {
            width: _gridview.cellWidth
            height: _gridview.cellHeight
        }
    }

    Connections {
        target: DiscountTableController
        onShow: root.visible = true
        onClose: root.visible = false
    }
}
