import QtQuick 2.0
import "qrc:///view/Control"

Item {
    id: root
    MyButton {
        text: modelData.name
        anchors {
            fill:parent
            margins: 5
        }
        onClicked: Route.post('DiscountTableController@c_setDiscount',{x: modelData.value})
    }
}
