#ifndef BILLSSTRUCT_H
#define BILLSSTRUCT_H

#include "checkoutstruct.h"

class BillsStruct
{
public:
    BillsStruct(int count = 10);
    void setCheckOutStruct(int index,const CheckOutStruct* check = nullptr);
    CheckOutStruct& getCheckOutStruct();
    int count();
    bool isEmpty(int index);
private:
    QMap<int,CheckOutStruct>::iterator m_curIter;
    QMap<int,CheckOutStruct> m_checkoutList;
};

#endif // BILLSSTRUCT_H
