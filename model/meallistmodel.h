#ifndef MEALLISTMODEL_H
#define MEALLISTMODEL_H

#include <QAbstractListModel>
#include "mealstruct.h"
#include "global.h"

class MealListModel : public QAbstractListModel
{
    Q_OBJECT
    enum {
        enum_mealId = Qt::UserRole + 1,
        enum_mealName,
        enum_end
    };
public:
    explicit MealListModel(QObject *parent = nullptr);

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    void updateMealList(const QList<MealStruct> mealList);
protected:
    QHash<int, QByteArray> roleNames() const;
private:
    QList<MealStruct> m_mealList;
};

#endif // MEALLISTMODEL_H
