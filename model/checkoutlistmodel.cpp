#include "checkoutlistmodel.h"
#include <QDebug>
CheckOutListModel::CheckOutListModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

int CheckOutListModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    auto checkOutList = g_billStruct.getCheckOutStruct();
    return checkOutList.mealList.count();
}

QVariant CheckOutListModel::data(const QModelIndex &index, int role) const
{
    if(index.row() < 0 || index.row() >= this->rowCount())
        return QVariant();

    auto mealItem = g_billStruct.getCheckOutStruct().mealList.at(index.row());
    switch(role) {
        case enum_mealId:
            return QVariant(mealItem.id);
        case enum_mealName:
            return QVariant(mealItem.name);
        case enum_count:
            return QVariant(mealItem.count);
        case enum_specialArray: {
            QString specialString = "";
            auto info = mealItem.infoList.at(0);
            auto sizeStr = info.getSizeStr();
            auto sugarStr = mealItem.sugar.name;
            auto iceStr = mealItem.ice.name;
            specialString += sugarStr + ',';
            specialString += iceStr + ',';
            specialString += sizeStr + ',';

            mealItem.addSpecialListForeach([&specialString](auto& specialItem){
                specialString += specialItem.name + ',';
            });

            mealItem.subSpecialListForeach([&specialString](auto& specialItem){
                specialString += specialItem.name + ',';
            });
            return QVariant(specialString);
        }
        case enum_price: {
            auto price = mealItem.getTotalPrice();
            return QVariant(price);
        }
    }
    return QVariant();
}

void CheckOutListModel::updateList() {
    beginResetModel();
    endResetModel();
}

QHash<int, QByteArray> CheckOutListModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[enum_mealId] = "mealId";
    roles[enum_mealName] = "mealName";
    roles[enum_specialArray] = "specialArray";
    roles[enum_count] = "count";
    roles[enum_price] = "totalPrice";
    return roles;
}
