#ifndef TYPELISTMODEL_H
#define TYPELISTMODEL_H

#include <QAbstractListModel>
#include "typestruct.h"


class TypeListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum {
        enum_typeId = Qt::UserRole + 1,
        enum_typeName,
        enum_end
    };
    explicit TypeListModel(QObject *parent = nullptr);

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    void setTypeList(const QList<TypeStruct> typeList);
protected:
    QHash<int, QByteArray> roleNames() const;
private:
    QList<TypeStruct> m_typeList;
};

#endif // TYPELISTMODEL_H
