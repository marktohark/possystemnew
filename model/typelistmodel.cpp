#include "typelistmodel.h"

TypeListModel::TypeListModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

int TypeListModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return m_typeList.count();
}

QVariant TypeListModel::data(const QModelIndex &index, int role) const
{
    if(index.row() < 0 || index.row() >= m_typeList.count())
        return QVariant();

    const TypeStruct& typeItem = m_typeList.at(index.row());
    if(role == enum_typeId)
        return QVariant(typeItem.id);
    else if(role == enum_typeName)
        return QVariant(typeItem.name);
    else
        return QVariant();

}

QHash<int, QByteArray> TypeListModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[enum_typeId] = "typeId";
    roles[enum_typeName] = "typeName";
    return roles;
}

void TypeListModel::setTypeList(const QList<TypeStruct> typeList) {
    beginResetModel();
    m_typeList = typeList;
    endResetModel();
}
