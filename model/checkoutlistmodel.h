#ifndef CHECKOUTLISTMODEL_H
#define CHECKOUTLISTMODEL_H

#include <QAbstractListModel>
#include "global.h"

class CheckOutListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum {
        enum_mealId = Qt::UserRole + 1,
        enum_mealName,
        enum_specialArray,
        enum_count,
        enum_price,
        enum_end
    };
    explicit CheckOutListModel(QObject *parent = nullptr);

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    void updateList();
protected:
    QHash<int, QByteArray> roleNames() const;
private:
};

#endif // CHECKOUTLISTMODEL_H
