#include "meallistmodel.h"
#include <qdebug.h>
MealListModel::MealListModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

int MealListModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return m_mealList.count();
}

QVariant MealListModel::data(const QModelIndex &index, int role) const
{
    if(index.row() < 0 || index.row() >= m_mealList.count())
        return QVariant();

    const MealStruct& mealItem = m_mealList.at(index.row());

    if(role == enum_mealId)
        return QVariant(mealItem.id);
    else if(role == enum_mealName)
        return QVariant(mealItem.name);
    return QVariant();
}

void MealListModel::updateMealList(const QList<MealStruct> mealList) {
    beginResetModel();
    m_mealList = mealList;
    endResetModel();
}

QHash<int, QByteArray> MealListModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[enum_mealId] = "mealId";
    roles[enum_mealName] = "mealName";
    return roles;
}
