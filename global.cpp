#include "global.h"
#define SEND_CUR_PROGRESS(x) progressObj += x + QString(';');
#define SHOW_ERROR_STACK() QMessageBox::about(nullptr,"",progressObj);

Route *g_route = nullptr;
CConfig g_config;
CSqlOperator g_db;
QList<MealStruct> g_mealList;
QList<TypeStruct> g_typeList;
CEpsonPrinter g_epsonPrinter;
CTscPrinter g_tscPrinter;
BillsStruct g_billStruct;
int g_number = 1;



CErrorDebug initSql() {
    auto record2Map = [](QSqlRecord record) -> QVariantMap {
        QVariantMap map;
        for(int i = 0; i < record.count(); i++) {
            QString key = record.fieldName(i);
            QVariant value = record.value(i);
            map.insert(key, value);
        }
        return map;
    };

    CErrorDebug err;
    QString progressObj = ""; //SEND_CUR_PROGRESS 需要

    g_config.readNumber(NUMBER_PATH,g_number);

    SEND_CUR_PROGRESS("初始化資料庫")
    err = g_db.setDbPath(DB_PATH,"PosSystemDB");
    if(!err.isSuccess()) {
        QMessageBox::about(nullptr,"","連接資料庫錯誤" + err.getLastErrorStr() + ',' + err.getLastErrorCode());
        SHOW_ERROR_STACK()
        return err;
    }
    SEND_CUR_PROGRESS("初始化成功")

    SEND_CUR_PROGRESS("開始組合資料")
    SEND_CUR_PROGRESS("組合MealStruct")
    g_db.setTableName("MealsTable");
    err = g_db.fetchData("");
    if(!err.isSuccess()) {
        QMessageBox::about(nullptr,"","讀取MealsTable錯誤" + err.getLastErrorStr() + ',' + err.getLastErrorCode());
        SHOW_ERROR_STACK()
        return err;
    }

    //id,name,class,typeId
    while(g_db.next())
        g_mealList.append(MealStruct(record2Map(g_db.getRecord())));

    //初始化冰塊跟糖
    for(MealStruct& mealItem : g_mealList)
        if(mealItem._class == mealItem.enum_drink) {
            mealItem.ice.name = "正常";
            mealItem.ice.isEmpty = false;
            mealItem.sugar.name = "正常";
            mealItem.sugar.isEmpty = false;
        }



    SEND_CUR_PROGRESS("組合MealInfo")
    g_db.setTableName("MealsInfoTable");
    for(MealStruct& mealItem : g_mealList) {
        err = g_db.fetchData("meal_id=" + QString::number(mealItem.id));
        if(!err.isSuccess()) {
            QMessageBox::about(nullptr,"","讀取MealInfoTable錯誤" + err.getLastErrorStr() + ',' + err.getLastErrorCode());
            SHOW_ERROR_STACK()
            return err;
        }

        while(g_db.next())
            mealItem.infoList.append(MealInfo(record2Map(g_db.getRecord())));
    }

    SEND_CUR_PROGRESS("取得所有Special")
    g_db.setTableName("specialTable");
    g_db.fetchData("");
    QMap<int,SpecialStruct> specialList;
    while(g_db.next())
        specialList[g_db.value("id").toInt()] = SpecialStruct(record2Map(g_db.getRecord()));

    SEND_CUR_PROGRESS("組合SpecialList")
    g_db.setTableName("MealHasSpecialTable");
    for(MealStruct& mealItem : g_mealList) {
        err = g_db.fetchData("meal_id=" + QString::number(mealItem.id));
        if(!err.isSuccess()) {
            QMessageBox::about(nullptr,"","讀取SpecialList錯誤" + err.getLastErrorStr() + ',' + err.getLastErrorCode());
            SHOW_ERROR_STACK()
            return err;
        }
        while(g_db.next()) {
            int special_id = g_db.value("special_id").toInt();
            SpecialStruct specialItem = specialList[special_id];
            specialItem.price = g_db.value("special_price").toInt();

            if(specialItem.mode == SpecialStruct::enum_add)
                mealItem.addSpecialList.append(specialItem);
            else if(specialItem.mode == SpecialStruct::enum_sub)
                mealItem.subSpecialList.append(specialItem);
        }
    }

    SEND_CUR_PROGRESS("讀取TypeList")
    g_db.setTableName("TypeTable");
    err = g_db.fetchData("");
    if(!err.isSuccess()) {
        QMessageBox::about(nullptr,"","讀取TypeList錯誤" + err.getLastErrorStr() + ',' + err.getLastErrorCode());
        SHOW_ERROR_STACK()
        return err;
    }
    while(g_db.next())
        g_typeList.append(TypeStruct(record2Map(g_db.getRecord())));
    SEND_CUR_PROGRESS("讀取TypeList成功")
    return CErrorDebug(true);
}


void initAll() {
    QString progressObj; //SEND_CUR_PROGRESS 需要
    SEND_CUR_PROGRESS("讀取config檔案")
    CErrorDebug err = g_config.init(CONFIG_PATH);
    if(!err.isSuccess()) {
        QMessageBox::about(nullptr,"","config讀取失敗:" + err.getLastErrorStr() + ',' + err.getLastErrorCode());
        SHOW_ERROR_STACK()
        return;
    }
    SEND_CUR_PROGRESS("讀取config成功")

    //初始化資料庫
    err = initSql();
    if(!err.isSuccess())
        return;

    //初始化發票機
    SEND_CUR_PROGRESS("初始化epson發票機")
    QVariantMap temp;
    g_epsonPrinter.setConfig(&g_config);
    while(true) {
        err = g_epsonPrinter.init(g_config.getVal("epson_port").toString());
        if(!err.isSuccess()) {
            QMessageBox::about(nullptr,"","發票機連接失敗:" + err.getLastErrorStr() + ',' + err.getLastErrorCode());
            QMessageBox::StandardButton retBtn =  (QMessageBox::StandardButton)QMessageBox::question(nullptr,"","發票機連接失敗，是否重新連接?",QMessageBox::Yes,QMessageBox::No);
            if(retBtn == QMessageBox::Yes)
                continue;
            else {
                QMessageBox::about(nullptr,"","程式將繼續執行");
                g_epsonPrinter.m_isConnect = false;
                return;
            }
        }
        else {
            g_epsonPrinter.m_isConnect = true;
            break;
        }
    }

    SEND_CUR_PROGRESS("連接發票機成功")

    //tsc 貼紙機
    SEND_CUR_PROGRESS("初始化貼紙機")
    g_tscPrinter.setConfig(&g_config);
    while(true) {
        err = g_tscPrinter.initPrinter(TSCDLL_PATH);
        if(!err.isSuccess()) {
            QMessageBox::about(nullptr,"","貼紙機連接失敗:" + err.getLastErrorStr() + ',' + err.getLastErrorCode());
            QMessageBox::StandardButton ret = (QMessageBox::StandardButton)QMessageBox::question(nullptr,"","貼紙機連接失敗，是否重新連接?",QMessageBox::Yes,QMessageBox::No);
            if(ret == QMessageBox::Yes)
                continue;
            else {
                QMessageBox::about(nullptr,"","程式將繼續執行");
                g_tscPrinter.m_isConnect = false;
                break;
            }
        }
        else {
            g_tscPrinter.m_isConnect = true;
            break;
        }
    }
    SEND_CUR_PROGRESS("成功連接貼紙機")
    temp.clear();
    temp["path"] = "qrc:///view/MainWindow.qml";
    g_route->post("MainController@loader",temp);
    return;
}
