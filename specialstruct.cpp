#include "specialstruct.h"

SpecialStruct::SpecialStruct(int _id, QString _name, int _price, ENUM_MODE _mode, bool _isEmpty)
    :id{_id}, name(_name), price(_price), mode(_mode), isEmpty(_isEmpty) {

}

SpecialStruct::SpecialStruct(QVariantMap data) {
    byMap(data);
}

QVariantMap SpecialStruct::toMap() { //{id:??,name:??,price:??}
    QVariantMap result;
    result["id"] = id;
    result["name"] = name;
    result["price"] = price;
    result["mode"] = (int)mode;
    result["isEmpty"] = isEmpty;
    return result;
}

void SpecialStruct::byMap(QVariantMap data) {
    auto iter = data.find("id");
    if(iter != data.end())
        id = (*iter).toInt();

    iter = data.find("name");
    if(iter != data.end())
        name = (*iter).toString();

    iter = data.find("price");
    if(iter != data.end())
        price = (*iter).toInt();

    iter = data.find("mode");
    if(iter != data.end())
        mode = (ENUM_MODE)(*iter).toInt();

    iter = data.find("isEmpty");
    if(iter != data.end())
        isEmpty = (*iter).toBool();
}
