#include "global.h"

int DiscountStruct::getPrice(MealStruct& meal) {
    int price = -1;
    switch (mode) {
    case enum_free:
        price = 0;
        break;
    case enum_discount: {//折
        int mealPrice = meal.infoList.at(0).price;
        int addPrice = 0;
        int subPrice = 0;
        meal.addSpecialListForeach([&](SpecialStruct& item){
            addPrice += item.price;
        });
        meal.subSpecialListForeach([&](SpecialStruct& item){
            subPrice += item.price;
        });
        price = int((mealPrice + addPrice + subPrice) * (x / 10.0f) + 0.5);
        break;
    }
    case enum_chargeBack: {
        int mealPrice = meal.infoList.at(0).price;
        int addPrice = 0;
        int subPrice = 0;
        meal.addSpecialListForeach([&](SpecialStruct& item){
            addPrice += item.price;
        });
        meal.subSpecialListForeach([&](SpecialStruct& item){
            subPrice += item.price;
        });
        price = int(mealPrice + addPrice + subPrice - x);
        break;
    }
    default:
        int mealPrice = meal.infoList.at(0).price;
        int addPrice = 0;
        int subPrice = 0;
        meal.addSpecialListForeach([&](SpecialStruct& item){
            addPrice += item.price;
        });
        meal.subSpecialListForeach([&](SpecialStruct& item){
            subPrice += item.price;
        });
        price = int(mealPrice + addPrice + subPrice);
        break;
    }
    return price * meal.count;
}
