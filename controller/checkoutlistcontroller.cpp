#include "checkoutlistcontroller.h"
#include <QDebug>
CheckOutListController::CheckOutListController(QObject *parent) : QObject(parent)
{

}

QVariantMap CheckOutListController::c_selectMeal(QVariantMap data) {
    int index = data["index"].toInt();
    m_curSelectedMealIndex = index;
    emit this->setSelectedIndex(index);
    return QVariantMap();
}

QVariantMap CheckOutListController::c_addButton(QVariantMap data) {
    auto curMeal = this->getCurMealStruct();
    if(curMeal == nullptr)
        return QVariantMap();

    curMeal->count++;
    this->c_updateList(QVariantMap());
    return QVariantMap();
}

QVariantMap CheckOutListController::c_subButton(QVariantMap data) {
    auto curMeal = this->getCurMealStruct();
    if(curMeal == nullptr)
        return QVariantMap();

    curMeal->count--;
    if(curMeal->count <= 0)
        curMeal->count = 1;
    this->c_updateList(QVariantMap());
    return QVariantMap();
}

QVariantMap CheckOutListController::c_freeButton(QVariantMap data) {
    auto curMeal = this->getCurMealStruct();
    if(curMeal == nullptr)
        return QVariantMap();

    curMeal->discount.mode = DiscountStruct::enum_free;
    this->c_updateList(QVariantMap());
    return QVariantMap();
}

QVariantMap CheckOutListController::c_copyButton(QVariantMap data) {
    if(!check())
        return QVariantMap();
    auto& curCheckOutList = g_billStruct.getCheckOutStruct();
    auto mealIter = curCheckOutList.mealList.begin() + m_curSelectedMealIndex;
    auto newMeal = curCheckOutList.mealList.insert(mealIter + 1,(*mealIter).copyClearObj());
    (*newMeal).count = 1;
    this->c_updateList(QVariantMap());
    emit this->moveToItem(++m_curSelectedMealIndex);
    return QVariantMap();
}

QVariantMap CheckOutListController::c_deleteButton(QVariantMap data) {
    if(!check())
        return QVariantMap();
    auto& curCheckOutList = g_billStruct.getCheckOutStruct();
    auto mealIter = curCheckOutList.mealList.begin() + m_curSelectedMealIndex;
    curCheckOutList.mealList.erase(mealIter);
    m_curSelectedMealIndex--;
    this->c_updateList(QVariantMap());
    emit this->moveToItem(m_curSelectedMealIndex);
    return QVariantMap();
}

QVariantMap CheckOutListController::c_getSelectedIndex(QVariantMap data) {
    QVariantMap result;
    result["index"] = m_curSelectedMealIndex;
    return result;
}

CheckOutListModel* CheckOutListController::getCheckOutListModel() {
    return &m_checkOutListModel;
}

QVariantMap CheckOutListController::c_updateList(QVariantMap data) {
    emit this->getCurScrollPos();
    float curPos = m_buffer.toFloat();
    m_checkOutListModel.updateList();
    emit this->setScrollToPos(curPos);
    g_route->post("DetailTableController@c_updateTotalMoney");
    g_route->post("DetailTableController@c_updateNumber");
    g_route->post("DetailTableController@c_updateTransfer");
    g_route->post("DetailTableController@c_updatePayMoney");
    return QVariantMap();
}

QVariantMap CheckOutListController::c_moveLastItem(QVariantMap data) {
    emit this->moveLastItem();
    return QVariantMap();
}

QVariantMap CheckOutListController::c_getCurScrollPos(QVariantMap data) {
    QVariantMap result;
    emit this->getCurScrollPos();
    result["pos"] = m_buffer;
    return result;
}

QVariantMap CheckOutListController::c_setScrollToPos(QVariantMap data) {
    float pos = data["pos"].toFloat();
    emit this->setScrollToPos(pos);
    return QVariantMap();
}

QVariantMap CheckOutListController::c_deleteCheckOut(QVariantMap data) {
    auto& checkOut = g_billStruct.getCheckOutStruct();
    checkOut.clear();
    this->m_curSelectedMealIndex = -1;
    g_route->post("CheckOutListController@c_updateList");
    return QVariantMap();
}

bool CheckOutListController::check() {
    if(m_curSelectedMealIndex < 0)
        return false;
    return true;
}

MealStruct* CheckOutListController::getCurMealStruct() {
    if(!check())
        return nullptr;
    auto& curCheckOutList = g_billStruct.getCheckOutStruct();
    auto curMealStruct = &curCheckOutList.mealList[m_curSelectedMealIndex];
    return curMealStruct;
}
