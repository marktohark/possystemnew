#ifndef MEALLISTCONTROLLER_H
#define MEALLISTCONTROLLER_H

#include <QObject>
#include "global.h"
#include "model/meallistmodel.h"

class MealListController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(MealListModel* mealListModel READ getMealListModel CONSTANT)
public:
    explicit MealListController(QObject *parent = nullptr);
    QVariantMap addMealToCheckOut(QVariantMap data);
private:
    MealListModel* getMealListModel();
signals:

public slots:
    void showMealListByTypeId(int typeId);
private:
    MealListModel m_mealListModel;
};

#endif // MEALLISTCONTROLLER_H
