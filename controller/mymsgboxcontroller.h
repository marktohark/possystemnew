#ifndef MYMSGBOXCONTROLLER_H
#define MYMSGBOXCONTROLLER_H

#include <QObject>
#include <QVariantMap>
#include <QEventLoop>

class MyMsgBoxController : public QObject
{
    Q_OBJECT
public:
    explicit MyMsgBoxController(QObject *parent = nullptr);
    QVariantMap c_show(QVariantMap data);
    QVariantMap c_close(QVariantMap data);
    QVariantMap c_emitFinish(QVariantMap data);
    QVariantMap c_getBtn(QVariantMap data);
signals:
    void show(QVariantMap data);
    void close();
    void finish();
public slots:
private:
    bool m_btn{false};
};

#endif // MYMSGBOXCONTROLLER_H
