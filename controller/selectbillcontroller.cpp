#include "selectbillcontroller.h"
#include <QDebug>
SelectBillController::SelectBillController(QObject *parent) : QObject(parent)
{

}

QVariantList SelectBillController::getBillListModel() {
    QVariantList model;
    for(int i = 0; i < g_billStruct.count(); i++)
        model.append(i);
    return model;
}

QVariantMap SelectBillController::c_isEmptyByIndex(QVariantMap data) {
    QVariantMap result;
    result["isEmpty"] = true;
    int index = data["index"].toInt();
    result["isEmpty"] = g_billStruct.isEmpty(index);
    return result;
}

QVariantMap SelectBillController::c_setCurBillByIndex(QVariantMap data) {
    int index = data["index"].toInt();
    g_billStruct.setCheckOutStruct(index);
    g_route->post("CheckOutListController@c_updateList");
    QVariantMap _data;
    _data["index"] = -1;
    g_route->post("CheckOutListController@c_selectMeal", _data);
    return QVariantMap();
}

QVariantMap SelectBillController::c_show(QVariantMap data) {
    emit this->show();
    return QVariantMap();
}

QVariantMap SelectBillController::c_close(QVariantMap data) {
    emit this->close();
    return QVariantMap();
}
