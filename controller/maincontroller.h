#ifndef MAINCONTROLLER_H
#define MAINCONTROLLER_H

#include <QObject>
#include <QVariantMap>

class MainController : public QObject
{
    Q_OBJECT
public:
    explicit MainController(QObject *parent = nullptr);
    QVariantMap loader(QVariantMap data);
signals:
    void loaderControl(QVariantMap qmlPath);
public slots:
};

#endif // MAINCONTROLLER_H
