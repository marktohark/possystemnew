#include "typelistcontroller.h"
#include "meallistcontroller.h"

TypeListController::TypeListController(QObject *parent) : QObject(parent)
{

}

QVariantMap TypeListController::c_updateList(QVariantMap data) {
    m_typeListModel.setTypeList(g_typeList);
    return QVariantMap();
}

QVariantMap TypeListController::c_emitTypeId(QVariantMap data) {
    emit this->sendTypeId(data["typeId"].toInt());
    return QVariantMap();
}

TypeListModel* TypeListController::getTypeListModel() {


    return &m_typeListModel;
}
