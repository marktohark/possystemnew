#ifndef LOADPANELCONTROLLER_H
#define LOADPANELCONTROLLER_H

#include <QObject>
#include <QVariantMap>
#include <QtConcurrent/QtConcurrent>
#include <QFuture>
#include "global.h"

class LoadPanelController : public QObject
{
    Q_OBJECT
public:
    explicit LoadPanelController(QObject *parent = nullptr);
    Q_INVOKABLE void initGlobal();
    QVariantMap c_sendText(QVariantMap data);
signals:
    void sendText(QVariant text);
public slots:
};

#endif // LOADPANELCONTROLLER_H
