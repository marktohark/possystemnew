#include "discounttablecontroller.h"

DiscountTableController::DiscountTableController(QObject *parent) : QObject(parent)
{

}

QVariantMap DiscountTableController::c_show(QVariantMap data) {
    emit this->show();
    return QVariantMap();
}

QVariantMap DiscountTableController::c_close(QVariantMap data) {
    emit this->close();
    return QVariantMap();
}

QVariantMap DiscountTableController::c_setDiscount(QVariantMap data) {
    int x = data["x"].toInt();
    int index = g_route->post("CheckOutListController@c_getSelectedIndex")["index"].toInt();
    if(index < 0) {
        emit this->close();
        return QVariantMap();
    }

    auto& curMealItem = g_billStruct.getCheckOutStruct().mealList[index];

    if(x > 0) {
        curMealItem.discount.mode = DiscountStruct::enum_discount;
        curMealItem.discount.x = x;
    }
    else {
        curMealItem.discount.mode = DiscountStruct::enum_non;
        curMealItem.discount.x = -1;
    }
    g_route->post("CheckOutListController@c_updateList");
    emit this->close();
    return QVariantMap();
}
