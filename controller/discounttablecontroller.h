#ifndef DISCOUNTTABLECONTROLLER_H
#define DISCOUNTTABLECONTROLLER_H

#include <QObject>
#include <QVariantMap>
#include "global.h"

class DiscountTableController : public QObject
{
    Q_OBJECT
public:
    explicit DiscountTableController(QObject *parent = nullptr);
    QVariantMap c_show(QVariantMap data);
    QVariantMap c_close(QVariantMap data);
    QVariantMap c_setDiscount(QVariantMap data);
signals:
    void show();
    void close();
};

#endif // DISCOUNTTABLECONTROLLER_H
