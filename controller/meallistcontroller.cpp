#include "meallistcontroller.h"
#include <QDebug>
MealListController::MealListController(QObject *parent) : QObject(parent)
{

}

QVariantMap MealListController::addMealToCheckOut(QVariantMap data) {
    int mealId = data["mealId"].toInt();
    for(MealStruct& mealItem : g_mealList)
        if(mealItem.id == mealId) {
            auto& cutCheckOut = g_billStruct.getCheckOutStruct();
            cutCheckOut.mealList.append(mealItem.copyClearObj());
            break;
        }
    g_route->post("CheckOutListController@c_updateList");
    g_route->post("CheckOutListController@c_moveLastItem");
    return QVariantMap();
}

MealListModel* MealListController::getMealListModel() {
    return &m_mealListModel;
}

void MealListController::showMealListByTypeId(int typeId) {
    QList<MealStruct> mealList;
    for(const MealStruct& mealItem : g_mealList)
        if(mealItem.typeId == typeId)
            mealList.append(MealStruct(mealItem.id,mealItem.name));

    m_mealListModel.updateMealList(mealList);
}
