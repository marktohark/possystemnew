#include "loadpanelcontroller.h"

LoadPanelController::LoadPanelController(QObject *parent) : QObject(parent)
{

}

QVariantMap LoadPanelController::c_sendText(QVariantMap data) {
    emit this->sendText(data["text"]);
    return QVariantMap();
}

void LoadPanelController::initGlobal() {
    initAll();
    //QFuture<void> future = QtConcurrent::run( initAll );
}
