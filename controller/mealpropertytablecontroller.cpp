#include "mealpropertytablecontroller.h"

MealPropertyTableController::MealPropertyTableController(QObject *parent) : QObject(parent)
{

}

QVariantMap MealPropertyTableController::c_setSugar(QVariantMap data) {
    QString sugarName = data["sugar"].toString();
    m_curMealItem->sugar.name = sugarName;
    m_curMealItem->sugar.price = 0;
    m_curMealItem->sugar.isEmpty = false;
    emit this->sugarUnChecked();
    g_route->post("CheckOutListController@c_updateList");
    return QVariantMap();
}

QVariantMap MealPropertyTableController::c_setIce(QVariantMap data) {
    QString iceName = data["ice"].toString();
    m_curMealItem->ice.name = iceName;
    m_curMealItem->ice.price = 0;
    m_curMealItem->ice.isEmpty = false;
    emit this->iceUnChecked();
    g_route->post("CheckOutListController@c_updateList");
    return QVariantMap();
}

QVariantMap MealPropertyTableController::c_show(QVariantMap data) {
    auto curIndex = g_route->post("CheckOutListController@c_getSelectedIndex")["index"].toInt();
    if(curIndex < 0) {
        m_curMealItem = nullptr;
        return QVariantMap();
    }
    auto& check = g_billStruct.getCheckOutStruct();
    m_curMealItem = &check.mealList[curIndex];
    emit this->setMealProperty(m_curMealItem->toMap());
    emit this->show();
    return QVariantMap();
}

QVariantMap MealPropertyTableController::c_close(QVariantMap data) {
    emit this->close();
    return QVariantMap();
}

QVariantMap MealPropertyTableController::c_sugarCheckedByValue(QVariantMap data) {
    emit this->sugarCheckedByValue(data["val"]);
    return QVariantMap();
}

QVariantMap MealPropertyTableController::c_getAddSpecialModel(QVariantMap data) {
    QVariantList addModel;
    QVariantMap result;
    auto s_curMealIterIter = findSourceMealItem(m_curMealItem->id);

    (*s_curMealIterIter).addSpecialListForeach([&](auto& specialItem){
        addModel.push_back(specialItem.toMap());
    });
    result["addSpecialList"] = addModel;
    return result;
}

QVariantMap MealPropertyTableController::c_getSubSpecialModel(QVariantMap data) {
    QVariantList subModel;
    QVariantMap result;
    auto s_curMealIterIter = findSourceMealItem(m_curMealItem->id);

    (*s_curMealIterIter).subSpecialListForeach([&](auto& specialItem){
        subModel.push_back(specialItem.toMap());
    });
    result["subSpecialList"] = subModel;
    return result;
}

QVariantMap MealPropertyTableController::c_getSizeModel(QVariantMap data) {
    QVariantList mealInfoList;
    QVariantMap result;
    auto s_curMealItemIter = findSourceMealItem(m_curMealItem->id);
    (*s_curMealItemIter).infoListForeach([&](auto& info){
        if(info.size >= 0)
            mealInfoList.push_back(info.toMap());
    });
    result["sizeModel"] = mealInfoList;
    return result;
}

QVariantMap MealPropertyTableController::c_setSpecial(QVariantMap data) {
    int specialId = data["specialId"].toInt();
    bool set = data["set"].toBool();
    QString mode = data["mode"].toString();
    this->setSpecial(specialId, set, mode);
    g_route->post("CheckOutListController@c_updateList");
    return QVariantMap();
}

QVariantMap MealPropertyTableController::c_setSize(QVariantMap data) {
    auto s_curItemMealItemIter = findSourceMealItem(m_curMealItem->id);
    MealInfo::ENUM_SIZE d_size = (MealInfo::ENUM_SIZE)data["size"].toInt();
    auto infoItemIter = std::find_if((*s_curItemMealItemIter).infoList.begin(),(*s_curItemMealItemIter).infoList.end(),[&](auto& infoItem){
        return infoItem.size == d_size;
    });
    m_curMealItem->infoList.clear();
    m_curMealItem->infoList.push_back((*infoItemIter));
    emit this->sizeUnChecked();
    g_route->post("CheckOutListController@c_updateList");
    return QVariantMap();
}

QVariantMap MealPropertyTableController::c_getCurSpecialList(QVariantMap data) {
    QVariantList specialList;
    QVariantMap result;
    QString mode = data["mode"].toString();
    auto func = [&](auto& specialItem){
        specialList.push_back(specialItem.toMap());
    };
    if(mode == "add")
        m_curMealItem->addSpecialListForeach(func);
    else if(mode == "sub")
        m_curMealItem->subSpecialListForeach(func);
    else
        return QVariantMap();

    result["specialList"] = specialList;
    return result;
}

QVariantMap MealPropertyTableController::c_setCheckBoxByExist(QVariantMap data) {
    emit this->setCheckBoxByExist();
    return QVariantMap();
}

void MealPropertyTableController::setSpecial(int specialId, bool set, QString mode) {
    auto s_curMealItemIter = findSourceMealItem(m_curMealItem->id);
    QList<SpecialStruct>* s_curSpecialList = nullptr;
    QList<SpecialStruct>* d_curSpecialList = nullptr;
    if(mode == "add") {
        s_curSpecialList = &(*s_curMealItemIter).addSpecialList;
        d_curSpecialList = &m_curMealItem->addSpecialList;
    }
    else if(mode == "sub") {
        s_curSpecialList = &(*s_curMealItemIter).subSpecialList;
        d_curSpecialList = &m_curMealItem->subSpecialList;
    }
    else
        return;

    if(set) {
        auto iter = std::find_if(s_curSpecialList->begin(),s_curSpecialList->end(),[&](SpecialStruct& specialItem){
            return specialItem.id == specialId;
        });
        d_curSpecialList->push_back(*iter);
    }
    else {
        auto iter = std::find_if(d_curSpecialList->begin(),d_curSpecialList->end(),[&](SpecialStruct& specialItem){
            return specialItem.id == specialId;
        });
        d_curSpecialList->erase(iter);
    }
}

QList<MealStruct>::iterator MealPropertyTableController::findSourceMealItem(int dest_mealId) {
    QList<MealStruct>::iterator result = std::find_if(g_mealList.begin(), g_mealList.end(), [&](auto& mealItem){
        return mealItem.id == dest_mealId;
    });
    return result;
}
