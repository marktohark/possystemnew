#ifndef DETAILTABLECONTROLLER_H
#define DETAILTABLECONTROLLER_H

#include <QObject>
#include "global.h"
#include <QVariantMap>

class DetailTableController : public QObject
{
    Q_OBJECT
public:
    explicit DetailTableController(QObject *parent = nullptr);
    QVariantMap c_updateTotalMoney(QVariantMap data);//更新總價格
    QVariantMap c_updateNumber(QVariantMap data); //更新號碼
    QVariantMap c_updatePayMoney(QVariantMap data); //更新付錢
    QVariantMap c_setTransfer(QVariantMap data);
    QVariantMap c_clearCheckBox(QVariantMap data);
    QVariantMap c_updateTransfer(QVariantMap data); //更新transfer
signals:
    void updateTotalMoney(int totalMoney);
    void updatePayMoney(int payMoney);
    void updateNumber(int number);
    void updateChange(int change);//找錢
    void clearCheckBox();
    void updateTransfer(QString transferStr);
public slots:
    void sendNumber(QString command);//用來接收keyBoard的數字
};

#endif // DETAILTABLECONTROLLER_H
