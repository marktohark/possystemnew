#ifndef CHECKOUTLISTCONTROLLER_H
#define CHECKOUTLISTCONTROLLER_H

#include <QObject>
#include <QVariantMap>
#include "global.h"
#include "model/checkoutlistmodel.h"

class CheckOutListController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariant Buffer MEMBER m_buffer)
    Q_PROPERTY(CheckOutListModel* checkOutListModel READ getCheckOutListModel CONSTANT)
public:
    explicit CheckOutListController(QObject *parent = nullptr);
    QVariantMap c_selectMeal(QVariantMap data);
    QVariantMap c_addButton(QVariantMap data);
    QVariantMap c_subButton(QVariantMap data);
    QVariantMap c_freeButton(QVariantMap data);
    QVariantMap c_copyButton(QVariantMap data);
    QVariantMap c_deleteButton(QVariantMap data);
    QVariantMap c_getSelectedIndex(QVariantMap data);
    QVariantMap c_updateList(QVariantMap data);
    QVariantMap c_moveLastItem(QVariantMap data);
    QVariantMap c_getCurScrollPos(QVariantMap data);
    QVariantMap c_setScrollToPos(QVariantMap data);
    QVariantMap c_deleteCheckOut(QVariantMap data);
private:
    CheckOutListModel* getCheckOutListModel();
    bool check();
    MealStruct* getCurMealStruct();
signals:
    void setSelectedIndex(int selectedIndex);
    void moveLastItem();
    void moveToItem(int toIndex);
    void getCurScrollPos();
    void setScrollToPos(float pos);
public:
    int m_curSelectedMealIndex{-1}; //CheckOutList當前選重的Meal -1，代表沒有選重
    QVariant m_buffer;
private:
    CheckOutListModel m_checkOutListModel;
};

#endif // CHECKOUTLISTCONTROLLER_H
