#include "mymsgboxcontroller.h"
#include <QDebug>

MyMsgBoxController::MyMsgBoxController(QObject *parent) : QObject(parent)
{

}

QVariantMap MyMsgBoxController::c_show(QVariantMap data) {
    emit this->show(data);
    return QVariantMap();
}

QVariantMap MyMsgBoxController::c_close(QVariantMap data) {
    emit this->close();
    return QVariantMap();
}

QVariantMap MyMsgBoxController::c_emitFinish(QVariantMap data) {
    m_btn = data["btn"].toBool();
    emit this->finish();
    return QVariantMap();
}

QVariantMap MyMsgBoxController::c_getBtn(QVariantMap data) {
    QEventLoop loop;
    connect(this,SIGNAL(finish()),&loop,SLOT(quit()));
    loop.exec();
    data["btn"] = m_btn;
    return data;
}
