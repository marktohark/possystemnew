#include "detailtablecontroller.h"
#include "sstream"
#include <QDebug>

DetailTableController::DetailTableController(QObject *parent) : QObject(parent)
{

}

void DetailTableController::sendNumber(QString command) {
    std::stringstream bufr("");
    auto& curCheck = g_billStruct.getCheckOutStruct();
    int& payMoney = curCheck.payMoney;
    if(command == "clr")
         payMoney = 0;
    else {
        bufr << payMoney << command.toStdString();
        bufr >> payMoney;
    }
    emit this->updatePayMoney(payMoney);
    emit this->updateChange(curCheck.payMoney - curCheck.totalPrice);
}

QVariantMap DetailTableController::c_updateTotalMoney(QVariantMap data) {
    auto& curCheck = g_billStruct.getCheckOutStruct();
    curCheck.calcMealTotalPrice();
    emit this->updateTotalMoney(curCheck.totalPrice);
    emit this->updateChange(curCheck.payMoney - curCheck.totalPrice);
    return QVariantMap();
}

QVariantMap DetailTableController::c_updateNumber(QVariantMap data) {
    emit this->updateNumber(g_number);
    return QVariantMap();
}

QVariantMap DetailTableController::c_updatePayMoney(QVariantMap data) {
    auto& curCheck = g_billStruct.getCheckOutStruct();
    this->updatePayMoney(curCheck.payMoney);
    return QVariantMap();
}

QVariantMap DetailTableController::c_setTransfer(QVariantMap data) {
    auto& check = g_billStruct.getCheckOutStruct();
    QString typeStr = data["typeStr"].toString();
    CheckOutStruct::ENUM_TRANSFER transfer = CheckOutStruct::enum_non;
    if(typeStr == "外送") {
        transfer = CheckOutStruct::enum_order;
    }
    else if(typeStr == "內用") {
        transfer = CheckOutStruct::enum_eatHere;
    }
    else if(typeStr == "自取") {
        transfer = CheckOutStruct::enum_takeSelf;
    }
    else if(typeStr == "外帶") {
        transfer = CheckOutStruct::enum_takeOut;
    }
    check.takeOut = transfer;
    return QVariantMap();
}

QVariantMap DetailTableController::c_clearCheckBox(QVariantMap data) {
    emit this->clearCheckBox();
    return QVariantMap();
}

QVariantMap DetailTableController::c_updateTransfer(QVariantMap data) {
    auto& curCheck = g_billStruct.getCheckOutStruct();
    emit this->updateTransfer(curCheck.getCurTransferStr());
    return QVariantMap();
}
