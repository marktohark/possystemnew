#ifndef KEYBOARDCONTROLLER_H
#define KEYBOARDCONTROLLER_H

#include <QObject>
#include <QVariantMap>

class KeyBoardController : public QObject
{
    Q_OBJECT
public:
    explicit KeyBoardController(QObject *parent = nullptr);
    QVariantMap c_sendButton(QVariantMap data);
signals:
    void sendButton(QString command);

public slots:
};

#endif // KEYBOARDCONTROLLER_H
