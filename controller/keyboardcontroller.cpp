#include "keyboardcontroller.h"
#include <QDebug>
KeyBoardController::KeyBoardController(QObject *parent) : QObject(parent)
{

}

QVariantMap KeyBoardController::c_sendButton(QVariantMap data) {
    QString command = data["command"].toString();
    emit this->sendButton(command);
    return QVariantMap();
}
