#include "functionalbuttoncontroller.h"

FunctionalButtonController::FunctionalButtonController(QObject *parent) : QObject(parent)
{

}

QVariantMap FunctionalButtonController::c_openDrawer(QVariantMap data) {
    g_epsonPrinter.openDrawer();
    return QVariantMap();
}

QVariantMap FunctionalButtonController::c_print(QVariantMap data) {
    auto& curCheck = g_billStruct.getCheckOutStruct();
    curCheck.number = g_number;
    g_number++;
    if(g_number > 9999)
        g_number = 1;
    g_config.saveNumber(NUMBER_PATH, g_number);
    if(g_epsonPrinter.m_isConnect)
        g_epsonPrinter.epson_print(curCheck);
    if(g_tscPrinter.m_isConnect)
        g_tscPrinter.printMeal(curCheck);
    g_route->post("CheckOutListController@c_deleteCheckOut");
    return QVariantMap();
}

QVariantMap FunctionalButtonController::c_addBag(QVariantMap data) {
    auto& curCheck = g_billStruct.getCheckOutStruct();
    MealStruct bagMeal(-1,"加購袋子",-1,-1,1);
    bagMeal.infoList.append(MealInfo(MealInfo::enum_non,1));
    curCheck.mealList.append(bagMeal);
    g_route->post("CheckOutListController@c_updateList");
    return QVariantMap();
}
