#ifndef MEALPROPERTYTABLE_H
#define MEALPROPERTYTABLE_H

#include <QObject>
#include <QVariantMap>
#include <QVariantList>
#include "global.h"
#include <algorithm>

class MealPropertyTableController : public QObject
{
    Q_OBJECT
public:
    explicit MealPropertyTableController(QObject *parent = nullptr);
    QVariantMap c_setSugar(QVariantMap data);
    QVariantMap c_setIce(QVariantMap data);
    QVariantMap c_show(QVariantMap data);
    QVariantMap c_close(QVariantMap data);
    QVariantMap c_sugarCheckedByValue(QVariantMap data);
    QVariantMap c_getAddSpecialModel(QVariantMap data);
    QVariantMap c_getSubSpecialModel(QVariantMap data);
    QVariantMap c_getSizeModel(QVariantMap data);
    QVariantMap c_setSpecial(QVariantMap data);
    QVariantMap c_setSize(QVariantMap data);
    QVariantMap c_getCurSpecialList(QVariantMap data);
    QVariantMap c_setCheckBoxByExist(QVariantMap data);
signals:
    void show();
    void close();
    void setMealProperty(QVariantMap mealProperty);
    void sugarUnChecked();
    void sugarCheckedByValue(QVariant val);
    void iceUnChecked();
    void iceCheckByValue(QVariant val);
    void sizeUnChecked();
    void sizeCheckByValue(QVariant val);
    void setCheckBoxByExist();
public slots:
private:
    void setSpecial(int specialId, bool set, QString mode);
    QList<MealStruct>::iterator findSourceMealItem(int dest_mealId);
private:
    MealStruct* m_curMealItem{nullptr};
};

#endif // MEALPROPERTYTABLE_H
