#ifndef TYPELISTCONTROLLER_H
#define TYPELISTCONTROLLER_H

#include <QObject>
#include <QVariantMap>
#include "global.h"
#include "model/typelistmodel.h"

class TypeListController : public QObject
{
    Q_OBJECT
    Q_PROPERTY(TypeListModel* typeListModel READ getTypeListModel CONSTANT)
public:
    explicit TypeListController(QObject *parent = nullptr);
    QVariantMap c_updateList(QVariantMap data);
    QVariantMap c_emitTypeId(QVariantMap data);
private:
    TypeListModel* getTypeListModel();
signals:
    void sendTypeId(int typeId);
public slots:
private:
    TypeListModel m_typeListModel;
};

#endif // TYPELISTCONTROLLER_H
