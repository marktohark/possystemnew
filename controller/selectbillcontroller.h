#ifndef SELECTBILLCONTROLLER_H
#define SELECTBILLCONTROLLER_H

#include <QObject>
#include <QVariantMap>
#include <global.h>

class SelectBillController : public QObject
{
    Q_PROPERTY(QVariantList getBillListModel READ getBillListModel CONSTANT)
    Q_OBJECT
public:
    explicit SelectBillController(QObject *parent = nullptr);
    QVariantList getBillListModel();

    QVariantMap c_isEmptyByIndex(QVariantMap data);
    QVariantMap c_setCurBillByIndex(QVariantMap data);
    QVariantMap c_show(QVariantMap data);
    QVariantMap c_close(QVariantMap data);
signals:
    void show();
    void close();
public slots:
};

#endif // SELECTBILLCONTROLLER_H
