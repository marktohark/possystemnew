#ifndef FUNCTIONALBUTTONCONTROLLER_H
#define FUNCTIONALBUTTONCONTROLLER_H

#include <QObject>
#include <QVariantMap>
#include "global.h"
class FunctionalButtonController : public QObject
{
    Q_OBJECT
public:
    explicit FunctionalButtonController(QObject *parent = nullptr);
    QVariantMap c_openDrawer(QVariantMap data);
    QVariantMap c_print(QVariantMap data);
    QVariantMap c_addBag(QVariantMap data);
signals:

public slots:
};

#endif // FUNCTIONALBUTTONCONTROLLER_H
