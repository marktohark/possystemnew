import QtQuick 2.10
import QtQuick.Controls 1.4
import "qrc:///view/Control"

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    visibility: "Maximized"
    color: "#000000"
    Loader {
        id:_loader
        width: parent.width
        height: parent.height
        source: "qrc:///view/Control/LoadPanel.qml"
    }

    MyMessageBox {

    }


    Connections {
        target: MainController
        onLoaderControl: _loader.setSource(qmlPath.path)
    }
}
