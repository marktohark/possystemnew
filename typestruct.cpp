#include "typestruct.h"

TypeStruct::TypeStruct(int _id, QString _name)
    :id(_id), name(_name) {

}

TypeStruct::TypeStruct(QVariantMap data) {
    byMap(data);
}

QVariantMap TypeStruct::toMap() {
    QVariantMap result;
    result["id"] = id;
    result["name"] = name;
    return result;
}

void TypeStruct::byMap(QVariantMap data) {
    auto iter = data.find("id");
    if(iter != data.end())
        id = (*iter).toInt();

    iter = data.find("name");
    if(iter != data.end())
        name = (*iter).toString();
}
