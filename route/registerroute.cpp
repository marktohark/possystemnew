#include "route.h"
#include "controller/maincontroller.h"
#include "controller/loadpanelcontroller.h"
#include "controller/mymsgboxcontroller.h"
#include "controller/typelistcontroller.h"
#include "controller/meallistcontroller.h"
#include "controller/selectbillcontroller.h"
#include "controller/checkoutlistcontroller.h"
#include "controller/keyboardcontroller.h"
#include "controller/detailtablecontroller.h"
#include "controller/functionalbuttoncontroller.h"
#include "controller/discounttablecontroller.h"
#include "controller/mealpropertytablecontroller.h"


RouteTableBegin
    RegisterControllerBegin(MainController)
        RegisterFunction(loader)
    RegisterControllerEnd

    RegisterControllerBegin(LoadPanelController)
        RegisterFunction(c_sendText)
    RegisterControllerEnd

    RegisterControllerBegin(MyMsgBoxController)
        RegisterFunction(c_show)
        RegisterFunction(c_close)
        RegisterFunction(c_emitFinish)
        RegisterFunction(c_getBtn)
    RegisterControllerEnd

    RegisterControllerBegin(MealListController)
        RegisterFunction(addMealToCheckOut)
    RegisterControllerEnd

    RegisterControllerBegin(TypeListController)
        RegisterFunction(c_updateList)
        RegisterFunction(c_emitTypeId)
    RegisterControllerEnd

    RegisterControllerBegin(SelectBillController)
        RegisterFunction(c_isEmptyByIndex)
        RegisterFunction(c_setCurBillByIndex)
        RegisterFunction(c_show)
        RegisterFunction(c_close)
    RegisterControllerEnd

    RegisterControllerBegin(CheckOutListController)
        RegisterFunction(c_selectMeal)
        RegisterFunction(c_addButton)
        RegisterFunction(c_subButton)
        RegisterFunction(c_getSelectedIndex)
        RegisterFunction(c_updateList)
        RegisterFunction(c_freeButton)
        RegisterFunction(c_copyButton)
        RegisterFunction(c_moveLastItem)
        RegisterFunction(c_deleteButton)
        RegisterFunction(c_getCurScrollPos)
        RegisterFunction(c_setScrollToPos)
        RegisterFunction(c_deleteCheckOut)
    RegisterControllerEnd

    RegisterControllerBegin(KeyBoardController)
        RegisterFunction(c_sendButton)
    RegisterControllerEnd

    RegisterControllerBegin(DetailTableController)
        RegisterFunction(c_updateNumber)
        RegisterFunction(c_setTransfer)
        RegisterFunction(c_clearCheckBox)
        RegisterFunction(c_updateTotalMoney)
        RegisterFunction(c_updateTransfer)
        RegisterFunction(c_updatePayMoney)
    RegisterControllerEnd

    RegisterControllerBegin(FunctionalButtonController)
        RegisterFunction(c_openDrawer)
        RegisterFunction(c_print)
        RegisterFunction(c_addBag)
    RegisterControllerEnd

    RegisterControllerBegin(DiscountTableController)
        RegisterFunction(c_show)
        RegisterFunction(c_close)
        RegisterFunction(c_setDiscount)
    RegisterControllerEnd

    RegisterControllerBegin(MealPropertyTableController)
        RegisterFunction(c_setIce)
        RegisterFunction(c_setSugar)
        RegisterFunction(c_show)
        RegisterFunction(c_close)
        RegisterFunction(c_sugarCheckedByValue)
        RegisterFunction(c_getAddSpecialModel)
        RegisterFunction(c_getSubSpecialModel)
        RegisterFunction(c_getSizeModel)
        RegisterFunction(c_setSpecial)
        RegisterFunction(c_setSize)
        RegisterFunction(c_getCurSpecialList)
        RegisterFunction(c_setCheckBoxByExist)
    RegisterControllerEnd
RouteTableEnd
