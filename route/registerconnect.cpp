#include "route.h"
#include "controller/typelistcontroller.h"
#include "controller/meallistcontroller.h"
#include "controller/keyboardcontroller.h"
#include "controller/detailtablecontroller.h"

ConnectTableBegin
    ConnectController_Signal2Slot(TypeListController,sendTypeId(int),
                                  MealListController,showMealListByTypeId(int))

    ConnectController_Signal2Slot(KeyBoardController,sendButton(QString),
                                  DetailTableController,sendNumber(QString))
ConnectTableEnd
