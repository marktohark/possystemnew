#include "route.h"
#include <QDebug>
QVariantMap Route::post(QString url,QVariantMap data) {
    return m_controllerFunc[url](data);
}

Route::~Route() {
    qDeleteAll(m_controller);
}

QObject* Route::getController(QString name) {
    return m_controller[name];
}
