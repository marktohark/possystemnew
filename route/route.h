#ifndef ROUTE_H
#define ROUTE_H

#include <QObject>
#include <QList>
#include <QDebug>
#include <QVariantMap>
#include <QVariantList>
#include <QVariant>
#include <QHash>
#include <QStringList>
#include <functional>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#define RegisterControllerBegin(Controller) { \
    auto ptr = new Controller(); \
    ctxt->setContextProperty(#Controller,ptr); \
    m_controller[#Controller] = ptr; \
    QString controllerName(#Controller);

#define RegisterControllerEnd }

#define RegisterFunction(Func) \
    m_controllerFunc[controllerName + "@"#Func] = [=](QVariantMap data)->QVariantMap { \
    return ptr->Func(data);\
    };\

#define RouteTableBegin Route::Route(QQmlApplicationEngine *pEngine,QObject *parent) : QObject(parent) { \
    this->pEngine = pEngine; \
    QQmlContext* ctxt = pEngine->rootContext();

#define RouteTableEnd this->initConnect();\
}

#define ConnectTableBegin void Route::initConnect() {

#define ConnectTableEnd }

#define ConnectController_Signal2Slot(signalCon,signalFunc,slotCon,slotFunc) \
{   auto pSignalCon = (signalCon*)this->m_controller[#signalCon]; \
    auto pSlotCon = (slotCon*)this->m_controller[#slotCon]; \
    QObject::connect(pSignalCon,SIGNAL(signalFunc),pSlotCon,SLOT(slotFunc));}

#define ConnectController_Signal2Signal(signalCon,signalFunc,slotCon,slotFunc) \
{   auto pSignalCon = (signalCon*)this->m_controller[#signalCon]; \
    auto pSlotCon = (slotCon*)this->m_controller[#slotCon]; \
    QObject::connect(pSignalCon,SIGNAL(signalFunc),pSlotCon,SIGNAL(slotFunc));}


class Route : public QObject
{
    Q_OBJECT
public:
    explicit Route(QQmlApplicationEngine *pEngine = nullptr,QObject *parent = nullptr);
    Q_INVOKABLE QVariantMap post(QString url,QVariantMap data = QVariantMap());
    QObject* getController(QString name);
    void initConnect();
    ~Route();
signals:


public slots:

private:
    QHash<QString,std::function<QVariantMap(QVariantMap)>> m_controllerFunc;
    QHash<QString,QObject*> m_controller;
    QQmlApplicationEngine *pEngine{nullptr};
};

#endif // ROUTE_H
