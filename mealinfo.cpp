#include "mealinfo.h"

MealInfo::MealInfo(int __size, int _price)
    :size((ENUM_SIZE)__size), price(_price), sizeStr(getSizeStr()) {

}

MealInfo::MealInfo(QVariantMap data) {
    byMap(data);
}

QVariantMap MealInfo::toMap() {
    QVariantMap result;
    result["size"] = (int)size;
    result["sizeStr"] = getSizeStr();
    result["price"] = price;
    return result;
}

void MealInfo::byMap(QVariantMap data) {
    auto iter = data.find("size");
    if(iter != data.end())
        size = (ENUM_SIZE)(*iter).toInt();

    iter = data.find("price");
    if(iter != data.end())
        price = (*iter).toInt();
}

QString MealInfo::getSizeStr() {
    if(size < 0)
        return "";
    else if(size == 0)
        return "小";
    else if(size == 1)
        return "中";
    else if(size == 2)
        return "大";
    return "";
}
