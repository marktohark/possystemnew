#include <QGuiApplication>
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QProcess>
#include "global.h"

#include "controller/checkoutlistcontroller.h"

int main(int argc, char *argv[])
{
#if defined(Q_OS_WIN)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QApplication app(argc, argv);

    QQmlApplicationEngine engine;

    g_route = new Route(&engine);
    QQmlContext *ctxt = engine.rootContext();
    ctxt->setContextProperty("Route", g_route);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;
    int appRet = app.exec();
    delete g_route;
    return appRet;
}
