#include "cconfig.h"

CConfig::CConfig(QObject *parent) : QObject(parent)
{

}

CErrorDebug CConfig::init(QString config_path) {
    QFile file(config_path);

    if(!file.open(QFile::ReadWrite))
        return CErrorDebug(false,"read: config can't open","0");

    QTextStream out(&file);

    QString configData;
    while(!out.atEnd()) {
        configData = out.readLine();
        if(configData.isEmpty())
            continue;

        QStringList strList = configData.split('=');
        if(strList.count() != 2)
            continue;
        m_config[strList[0]] = QVariant(strList[1]);
    }
    file.close();
    return CErrorDebug(true);
}

QVariant CConfig::getVal(QString key) {
    return m_config[key];
}

QVariantMap& CConfig::getRawMap() {
    return m_config;
}

CErrorDebug CConfig::readNumber(QString number_path, int& number) {
    QFile file(number_path);
    if(!file.open(QFile::ReadWrite))
        return CErrorDebug(false,"read: number can't open","0");

    QTextStream out(&file);
    out >> number;
    file.close();
    return CErrorDebug(true);
}

CErrorDebug CConfig::saveNumber(QString number_path, int& number) {
    QFile file(number_path);
    if(!file.open(QFile::ReadWrite))
        return CErrorDebug(false,"write: number can't open","0");

    QTextStream out(&file);
    out << number;
    file.close();
    return CErrorDebug(true);
}
