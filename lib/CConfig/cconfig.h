#ifndef CCONFIG_H
#define CCONFIG_H

#include <QObject>
#include <QVariantMap>
#include <QFile>
#include <QTextStream>
#include "lib/CErrorDebug/cerrordebug.h"

class CConfig : public QObject
{
    Q_OBJECT
public:
    explicit CConfig(QObject *parent = nullptr);
    CErrorDebug init(QString config_path);
    QVariant getVal(QString key);
    QVariantMap& getRawMap();
    CErrorDebug saveNumber(QString number_path, int &number);
    CErrorDebug readNumber(QString number_path, int &number);
signals:

public slots:
private:
    QVariantMap m_config;
};

#endif // CCONFIG_H
