#ifndef CSQLOPERATOR_H
#define CSQLOPERATOR_H

#include <QObject>
#include <QVariantMap>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlError>
#include <QSqlRecord>
#include <QSqlField>
#include <QVector>
#include <QDebug>
#include <QMessageBox>
#include "lib/CErrorDebug/cerrordebug.h"

class CSqlOperator :public QObject
{
    Q_OBJECT
public:
    CSqlOperator(QObject *parent = nullptr);
    Q_INVOKABLE CErrorDebug fetchData(QString condition);
    Q_INVOKABLE CErrorDebug deleteData(QString condition);
    Q_INVOKABLE CErrorDebug insertData(QVariantList rowArr);
    /*
        [[colName1,colName2...] , [colName1,colName2...]]
    */
    Q_INVOKABLE CErrorDebug insertDataByMap(QVariantMap rowData);
    /*
         {column1: val1, column2: val2, column3: val3..}
    */
    Q_INVOKABLE int getListInsertId();
    Q_INVOKABLE CErrorDebug updateData(QString condition, QVariantMap rowData);


    Q_INVOKABLE CErrorDebug setTableName(QString tableName);
    Q_INVOKABLE CErrorDebug setDbPath(QString path,QString connectName);

    bool next();
    QVariant value(int index);
    QVariant value(QString name);

    QSqlRecord getRecord();

private:
    QString m_dbPath{""};
    QString m_tableName{""};
    QSqlDatabase m_db;
    QSqlQuery m_sqlQueryData;
    QVector<QString> m_columnName;
    int m_lastInsertId{-1};
};

#endif // CSQLOPERATOR_H
