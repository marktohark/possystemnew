#include "csqloperator.h"

CSqlOperator::CSqlOperator(QObject *parent)
    :QObject(parent)
{

}

CErrorDebug CSqlOperator::fetchData(QString condition) {
    QSqlQuery query(m_db);
    bool ret = false;
    QString sql = "select * from " + m_tableName;
    if(!condition.isEmpty())
        sql += " where " + condition;

    query.prepare(sql);
    ret = query.exec();
    m_sqlQueryData = query;
    return CErrorDebug(ret,query.lastError().text(),query.lastError().nativeErrorCode());
}

CErrorDebug CSqlOperator::deleteData(QString condition) {
    QSqlQuery query(m_db);

    bool ret = query.exec("delete from " + m_tableName + " where " + condition);

    return CErrorDebug(ret,query.lastError().text(),query.lastError().nativeErrorCode());
}

CErrorDebug CSqlOperator::insertData(QVariantList rowArr) {
    if(rowArr.empty())
        return false;
    QSqlQuery query(m_db);

    QString sqlLanguage = "insert into " + m_tableName + " ";
    QString sqlValue = " values ";

    sqlValue += "(";
    for(auto i = rowArr.begin(); i != rowArr.end();) {
        QVariantList _row = (*i).value<QVariantList>();
        for(auto j = _row.begin(); j != _row.end();) {
            QString data = (*j).value<QString>();
            sqlValue += (data.isEmpty()?"null":"\"" + data + "\"");
            j++;
            if(j != _row.end())
                sqlValue += ',';
        }
        sqlValue += ")";
        i++;
        if(i != rowArr.end())
            sqlValue += ",(";
    }
    sqlLanguage += sqlValue;
    bool ret = query.exec(sqlLanguage);
    m_lastInsertId = query.lastInsertId().toInt();
    return CErrorDebug(ret,query.lastError().text(),query.lastError().nativeErrorCode());
}

CErrorDebug CSqlOperator::insertDataByMap(QVariantMap rowData) {
    if(rowData.empty())
        return false;

    QSqlQuery query(m_db);

    QString sqlLanguage = "insert into " + m_tableName + " (";
    QString sqlValue = " values (";

    for(auto i = rowData.begin(); i != rowData.end();) {
        sqlLanguage = sqlLanguage + i.key();
        sqlValue = sqlValue + '\"' + i.value().toString() + '\"';
        i++;
        if(i != rowData.end()) {
            sqlLanguage += ',';
            sqlValue += ',';
        }
        else {
            sqlLanguage += ')';
            sqlValue += ')';
        }
    }
    sqlLanguage += sqlValue;
    bool ret = query.exec(sqlLanguage);
    m_lastInsertId = query.lastInsertId().toInt();
    return CErrorDebug(ret,query.lastError().text(),query.lastError().nativeErrorCode());
}

int CSqlOperator::getListInsertId() {
    return m_lastInsertId;
}

CErrorDebug CSqlOperator::updateData(QString condition, QVariantMap rowData) {
    if(rowData.empty())
        return false;

    QSqlQuery query(m_db);

    QString sqlLanguage = "update " + m_tableName + " ";
    QString sqlValue = "set ";
    QString sqlCondition = (condition.isEmpty() ? "" : " where " + condition);

    for(auto i = rowData.begin(); i != rowData.end();) {
        sqlValue = sqlValue + i.key() + "=\"" + i.value().toString() + '\"';
        i++;
        if(i != rowData.end())
            sqlValue += ',';
    }
    sqlLanguage = sqlLanguage + sqlValue + sqlCondition;
    bool ret = query.exec(sqlLanguage);
    return CErrorDebug(ret,query.lastError().text(),query.lastError().nativeErrorCode());
}

CErrorDebug CSqlOperator::setTableName(QString tableName) {
    m_tableName = tableName;
    QSqlRecord record = m_db.record(m_tableName);
    for(int i = 0; i < record.count(); i++)
        m_columnName.push_back(record.field(i).name());
    return CErrorDebug(true);
}

CErrorDebug CSqlOperator::setDbPath(QString path,QString connectName) {
    m_dbPath = path;
    m_db = QSqlDatabase::addDatabase("QSQLITE",connectName);
    m_db.setDatabaseName(m_dbPath);
    if(!m_db.open())
        return CErrorDebug(false,m_db.lastError().text(),m_db.lastError().nativeErrorCode());

    return CErrorDebug(true);
}

bool CSqlOperator::next() {
    return m_sqlQueryData.next();
}

QVariant CSqlOperator::value(int index) {
    return m_sqlQueryData.value(index);
}

QVariant CSqlOperator::value(QString name) {
    return m_sqlQueryData.value(name);
}

QSqlRecord CSqlOperator::getRecord() {
    return m_sqlQueryData.record();
}
