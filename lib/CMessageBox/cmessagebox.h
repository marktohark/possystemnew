#ifndef CMESSAGEBOX_H
#define CMESSAGEBOX_H

#include <QObject>
#include <QWidget>
#include <QMessageBox>
#include <QInputDialog>
#include <QVariantMap>
#include "lib/CErrorDebug/cerrordebug.h"
#include <QString>

class CMessageBox: public QObject
{
    Q_OBJECT
    Q_PROPERTY(uint Ok MEMBER m_ok)
    Q_PROPERTY(uint Open MEMBER m_open)
    Q_PROPERTY(uint Save MEMBER m_save)
    Q_PROPERTY(uint Canel MEMBER m_canel)
    Q_PROPERTY(uint Close MEMBER m_close)
    Q_PROPERTY(uint Discard MEMBER m_discard)
    Q_PROPERTY(uint Apply MEMBER m_apply)
    Q_PROPERTY(uint Reset MEMBER m_reset)
    Q_PROPERTY(uint RestoreDefault MEMBER m_restoredefaults)
    Q_PROPERTY(uint Help MEMBER m_help)
    Q_PROPERTY(uint SaveAll MEMBER m_saveall)
    Q_PROPERTY(uint Yes MEMBER m_yes)
    Q_PROPERTY(uint YesToAll MEMBER m_yestoall)
    Q_PROPERTY(uint No MEMBER m_no)
    Q_PROPERTY(uint NoToAll MEMBER m_notoall)
    Q_PROPERTY(uint Abort MEMBER m_abort)
    Q_PROPERTY(uint Retry MEMBER m_retry)
    Q_PROPERTY(uint Ignore MEMBER m_ignore)
    Q_PROPERTY(uint NoButoon MEMBER m_nobutton)
public:
    CMessageBox(QObject *parent = nullptr);
public:
    Q_INVOKABLE void about(QString title="",QString text="");
    Q_INVOKABLE uint critical(QString title = "",QString text = "",uint buttons = QMessageBox::Yes | QMessageBox::No);
    Q_INVOKABLE uint question(QString title = "",QString text = "",uint buttons = QMessageBox::Yes | QMessageBox::No);
    Q_INVOKABLE uint information(QString title = "",QString text = "",uint buttons = QMessageBox::Yes | QMessageBox::No);
    Q_INVOKABLE uint warning(QString title = "",QString text = "",uint buttons = QMessageBox::Yes | QMessageBox::No);
    //{val:"xxx",ok:"false or true"}
    Q_INVOKABLE QVariantMap getDouble(QString title="",QString label="",double defaultVal = 0,double min = -2147483647, double max = 2147483647);
    Q_INVOKABLE QVariantMap getInt(QString title="",QString label="",int defaultVal = 0,int min = -2147483647,int max = 2147483647);
    Q_INVOKABLE QVariantMap getMultiLineText(QString title="",QString label="",QString defaultText="");
    Q_INVOKABLE QVariantMap getText(QString title="",QString label="",QString defaultText="");
    Q_INVOKABLE QVariantMap getItem(QString title="",QString label="",QVariantList item = QVariantList());
private: //Buttons Enum
    uint m_ok{QMessageBox::Ok};
    uint m_open{QMessageBox::Open};
    uint m_save{QMessageBox::Save};
    uint m_canel{QMessageBox::Cancel};
    uint m_close{QMessageBox::Close};
    uint m_discard{QMessageBox::Discard};
    uint m_apply{QMessageBox::Apply};
    uint m_reset{QMessageBox::Reset};
    uint m_restoredefaults{QMessageBox::RestoreDefaults};
    uint m_help{QMessageBox::Help};
    uint m_saveall{QMessageBox::SaveAll};
    uint m_yes{QMessageBox::Yes};
    uint m_yestoall{QMessageBox::YesToAll};
    uint m_no{QMessageBox::No};
    uint m_notoall{QMessageBox::NoToAll};
    uint m_abort{QMessageBox::Abort};
    uint m_retry{QMessageBox::Retry};
    uint m_ignore{QMessageBox::Ignore};
    uint m_nobutton{QMessageBox::NoButton};
};

#endif // CMESSAGEBOX_H
