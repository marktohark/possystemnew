#include "cmessagebox.h"

CMessageBox::CMessageBox(QObject *parent)
    :QObject(parent)
{

}

void CMessageBox::about(QString title,QString text) {
    QMessageBox::about(nullptr,title,text);
}

uint CMessageBox::question(QString title,QString text,uint buttons) {
    return QMessageBox::question(nullptr,title,text,QMessageBox::StandardButtons(buttons));
}

uint CMessageBox::critical(QString title,QString text,uint buttons) {
    return QMessageBox::critical(nullptr,title,text,QMessageBox::StandardButtons(buttons));
}

uint CMessageBox::information(QString title,QString text,uint buttons) {
    return QMessageBox::information(nullptr,title,text,QMessageBox::StandardButtons(buttons));
}

uint CMessageBox::warning(QString title,QString text,uint buttons) {
    return QMessageBox::warning(nullptr,title,text,QMessageBox::StandardButtons(buttons));
}

QVariantMap CMessageBox::getDouble(QString title,QString label,double defaultVal,double min, double max) {
    QVariantMap ret;
    bool _ok;
    double val = QInputDialog::getDouble(nullptr,title,label,defaultVal,min,max,1,&_ok);
    ret["val"] = val;
    ret["ok"] = _ok;
    return ret;
}

QVariantMap CMessageBox::getInt(QString title,QString label,int defaultVal,int min,int max) {
    QVariantMap ret;
    bool _ok;
    int val  = QInputDialog::getInt(nullptr,title,label,defaultVal,min,max,1,&_ok);
    ret["val"] = val;
    ret["ok"] = _ok;
    return ret;
}

QVariantMap CMessageBox::getMultiLineText(QString title,QString label,QString defaultText) {
    QVariantMap ret;
    bool _ok;
    QString val = QInputDialog::getMultiLineText(nullptr,title,label,defaultText,&_ok);
    ret["val"] = val;
    ret["ok"] = _ok;
    return ret;
}

QVariantMap CMessageBox::getText(QString title,QString label,QString defaultText) {
    QVariantMap ret;
    bool _ok;
    QString val = QInputDialog::getText(nullptr,title,label,QLineEdit::Normal,defaultText,&_ok);
    ret["val"] = val;
    ret["ok"] = _ok;
    return ret;
}

QVariantMap CMessageBox::getItem(QString title,QString label,QVariantList item) {
    QVariantMap ret;
    QStringList list;
    bool _ok;

    for(auto i = item.begin(); i != item.end(); i++)
        list.push_back((*i).value<QString>());

    QString val = QInputDialog::getItem(nullptr,title,label,list,0,false,&_ok);
    ret["val"] = val;
    ret["ok"] = _ok;
    return ret;
}
