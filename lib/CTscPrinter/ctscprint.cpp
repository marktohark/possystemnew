#include "lib/CTscPrinter/ctscprint.h"
#include <QDebug>
CTscPrinter::CTscPrinter(QObject *parent) : QObject(parent)
{

}

void CTscPrinter::setConfig(CConfig* ptr) {
    m_config = ptr;
}

CErrorDebug CTscPrinter::initPrinter(QString dllPath) {
    m_offset_x = m_config->getVal("tsc_offset_x").toInt();
    m_offset_y =  m_config->getVal("tsc_offset_y").toInt();
    m_print_w = m_config->getVal("tsc_print_width").toInt();
    m_print_h = m_config->getVal("tsc_print_heigh").toInt();
    m_chinese_len = m_config->getVal("tsc_chinese_word").toInt();
    m_english_len = m_config->getVal("tsc_english_word").toInt();
    CErrorDebug err = m_tscApi.initFuncPointer(dllPath);
    if(!err.isSuccess())
        return err;

    err = m_tscApi.openport(m_config->getVal("tsc_port").toString().toStdString().c_str());
    if(!err.isSuccess())
        return err;

    err = m_tscApi.clearbuffer();
    if(!err.isSuccess())
        return err;

    err = m_tscApi.setup("37.5", "24.38", "2", "10", "0", "3.5", "0");
    if(!err.isSuccess())
        return err;

    /*err = m_tscApi.formfeed();
    if(!err.isSuccess())
        return err;*/

    m_fontCode = QTextCodec::codecForName("big5");
    return CErrorDebug(true);
}

void CTscPrinter::closePort() {
    m_tscApi.clearbuffer();
    m_tscApi.closeport();
}

void CTscPrinter::printMeal(CheckOutStruct& checkout) {
    //int index = 1;
    checkout.mealLiistForeach([&](MealStruct& mealItem){
        textLeft(checkout.getCurTransferStr(),0,30);
        textRight("單號: " + QString::number(checkout.number),0,36);
        textLeft(mealItem.name + QString("%1元").arg(mealItem.getSinglePrice()),1.5,36);
        textLeft(">>" + mealItem.sugar.name + ',' + mealItem.ice.name + ',' + mealItem.infoList[0].getSizeStr(), 3);
        //textRight(QString::number(index),3);

        QString temp = ">>";
        mealItem.addSpecialListForeach([&](SpecialStruct& specialItem){
            temp += specialItem.name + ',';
        });
        if(temp.length() != 2)
            textLeft(">>" + temp, 4);

        temp = ">>";
        mealItem.subSpecialListForeach([&](SpecialStruct& specialItem){
            temp += specialItem.name + ',';
        });
        if(temp.length() != 2)
            textLeft(">>" + temp, 5);

        textLeft(m_config->getVal("data_storeName").toString(),7);
        textMid(m_config->getVal("data_storePhone").toString(),7);

        this->pirntLabel(mealItem.count);
    });
}

void CTscPrinter::textMid(QString text,float line, int sz) {
    float totalLen = getWordLength(text,sz);
    int begin_x = (int)((m_print_w - totalLen) / 2.0f + 0.5);
    int begin_y = line * m_chinese_len;
    QByteArray font = text.toUtf8();

    m_tscApi.windowsfont(m_offset_x + begin_x, m_offset_y + begin_y, sz, 0, 0, 0, "Arial", m_fontCode->fromUnicode(font).toStdString().c_str());
}

void CTscPrinter::textLeft(QString text,float line,int sz) {
    int begin_y = line * m_chinese_len;
    QByteArray font = text.toUtf8();
    m_tscApi.windowsfont(m_offset_x, m_offset_y + begin_y, sz, 0, 0, 0, "Arial", m_fontCode->fromUnicode(font).toStdString().c_str());
}

void CTscPrinter::textRight(QString text,float line,int sz) {
    float totalLen = getWordLength(text,sz);
    int begin_x = m_print_w - totalLen + 0.5;
    int begin_y = line * m_chinese_len;
    QByteArray font = text.toUtf8();
    m_tscApi.windowsfont(m_offset_x + begin_x, m_offset_y + begin_y, sz, 0, 0, 0, "Arial", m_fontCode->fromUnicode(font).toStdString().c_str());
}

float CTscPrinter::getWordLength(QString word,int sz) {
    uint en = 0,ch = 0;

    for(const auto &_c : word) { //c++11 support
        if(0x4E00 <= _c && _c <= 0x9FA5)
            ch++;
        else
            en++;
    }

    return ((ch * m_chinese_len + en * m_english_len) * (sz / 24.0f));
}

void CTscPrinter::pirntLabel(int count) {
    m_tscApi.printlabel("1", QString::number(count).toStdString().c_str());
    m_tscApi.clearbuffer();
}
