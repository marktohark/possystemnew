#ifndef CTscPrinter_H
#define CTscPrinter_H

#include <QObject>
#include <QVariantMap>
#include <QTextCodec>
#include <QtConcurrent/QtConcurrent>
#include "lib/CTscPrinterApi/ctscprinterapi.h"
#include "lib/CErrorDebug/cerrordebug.h"
#include "lib/CConfig/cconfig.h"
#include "checkoutstruct.h"

class CTscPrinter : public QObject
{
    Q_OBJECT
public:
    explicit CTscPrinter(QObject *parent = nullptr);
    void setConfig(CConfig* ptr);
    CErrorDebug initPrinter(QString dllPath = "");
    void closePort();
    ~CTscPrinter() {closePort();}

    void printMeal(CheckOutStruct& checkout);
signals:

private:
    void textMid(QString text,float line,int sz = 24); //至中
    void textLeft(QString text,float line,int sz = 24); //靠左
    void textRight(QString text,float line,int sz = 24); //靠右
    float getWordLength(QString word,int sz = 24); //第幾行
    void pirntLabel(int count = 1);//列印label

private:
    CTscPrinterApi m_tscApi;
    QTextCodec* m_fontCode;
    int m_offset_x{0};
    int m_offset_y{0};
    int m_print_w{0};
    int m_print_h{0};
    int m_chinese_len{0};
    int m_english_len{0};
    CConfig *m_config;
public:
    bool m_isConnect{false};
};

#endif // CTscPrinter_H
