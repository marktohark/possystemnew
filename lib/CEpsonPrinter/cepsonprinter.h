#ifndef CEpsonPrinter_H
#define CEpsonPrinter_H

#include <QObject>
#include "lib/CEpsonPrinterApi/cepsonprinterapi.h"
#include "lib/CErrorDebug/cerrordebug.h"
#include "lib/CConfig/cconfig.h"
#include "checkoutstruct.h"
#include <QDebug>

class CheckOutStruct;

class CEpsonPrinter : public QObject
{
    Q_OBJECT
public:
    explicit CEpsonPrinter(QObject *parent = nullptr);
    void setConfig(CConfig* ptr);
    CErrorDebug init(QString port); // config,com porrt

    void epson_print(CheckOutStruct& checkOut);
    void openDrawer(); //打開錢櫃

signals:

public slots:
private:
    void addLeftFont(QString text,uchar sz = 0); //增加靠左文字
    void addCenterFont(QString text,uchar sz = 0); //增加置中文字
    void addRightFont(QString text,uchar sz = 0); //增加靠又文字
    void addSpaceInFontMid(QString leftText,QString rightText,uchar sz = 0); //增加一段靠左 一段靠右 文字
    void addThreeList(QString leftText,QString midText,QString rightText,uchar leftLen,uchar midLen,uchar rightLen);
    void addFullFont(QChar _c,uchar sz = 0); // 使用一個字元，排滿整行//分隔線用，禁止中文
    void addLine(ushort count);

    ushort countOfWordLength(QString text); //計算QString共有多少length(英文跟中文不同dot)
    ushort countOfWordDot(QString text); //計算QString共有多少dot(英文跟中文不同dot)
    ushort centerSpacing(ushort wordLength,uchar sz = 0);//返回 置中時，左邊的空白要有幾個dot  sz=0 正常大小 =1 兩倍大 ...
    ushort rightSpacing(ushort wordLength,uchar sz = 0); //返回 靠右時，左邊的空白要有幾個dot  sz=0 正常大小 =1 兩倍大 ...
    QByteArray oneLineRemainSpacing(ushort wordLength ,uchar sz = 0); //一行裡面剩下多少空間，返回0x20(空白建) 填滿
    QByteArray makeSpacing(uint count = 0); //製造空白
public:
    CEpsonPrinterApi m_epsonPrinter;
    CConfig* m_config;
    bool m_isConnect{false};
};

#endif // CEpsonPrinter_H
