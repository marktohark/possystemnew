#include "cepsonprinter.h"
#include <QMessageBox>

CEpsonPrinter::CEpsonPrinter(QObject *parent) : QObject(parent)
{

}

void CEpsonPrinter::setConfig(CConfig* ptr) {
    m_config = ptr;
}

CErrorDebug CEpsonPrinter::init(QString port) {
    CErrorDebug err;
    m_epsonPrinter.setPortName(port);
    err = m_epsonPrinter.openSerialPort(QSerialPort::ReadWrite);
    if(!err.isSuccess())
        return err;
    return CErrorDebug(true);
}

void CEpsonPrinter::epson_print(CheckOutStruct& checkOut) {
    const QVariantMap& _config = m_config->getRawMap();

    m_epsonPrinter
            .c_init()
            .c_setPrintX(_config["epson_marginLeft_dot"].toFloat()) //左邊開始的點
            .c_setPrintWidth(_config["epson_printWidth"].toFloat()); //可列印的範圍

    addCenterFont(_config["data_storeName"].toString().toUtf8(),1);
    addLine(1);
    addSpaceInFontMid("單號" + QString("%1").arg(checkOut.number), checkOut.getCurTransferStr(), 1);
    addLine(1);
    addThreeList("餐點名稱","數量","單價",20,6,6);
    addFullFont('-');

    checkOut.mealLiistForeach([&](MealStruct& mealItem) {
        addThreeList(
                    mealItem.name + "(" + mealItem.infoList[0].getSizeStr() + ")",
                    "x" + QString::number(mealItem.count),
                    QString::number(mealItem.getTotalPrice()),
                    20, 6, 6);
        //糖冰塊
        QString tempStr = ">>";
        if(!mealItem.sugar.isEmpty)
            if(mealItem.sugar.name != "正常")
                tempStr += mealItem.sugar.name + ',';
        if(!mealItem.ice.isEmpty)
            if(mealItem.ice.name != "正常")
                tempStr += mealItem.ice.name + ',';
        if(tempStr != ">>")
            addSpaceInFontMid(tempStr, ""/*"+0"*/);

        //conaine
        int count = 0;
//        int price = 0;
        auto combineFunc = [&](SpecialStruct& specialItem){
            tempStr += specialItem.name + ',';
//            price += specialItem.price;
            count++;
            if(count % 5 == 0) {
                addSpaceInFontMid(tempStr, ""/* + QString::number(price)*/);
                tempStr = ">>";
//                price = 0;
                count = 0;
            }
        };
        tempStr = ">>";

        //add
        mealItem.addSpecialListForeach(combineFunc);

        //sub
        mealItem.subSpecialListForeach(combineFunc);

        if(tempStr.length() != 2)
            addSpaceInFontMid(tempStr, ""/* + QString::number(price)*/);
    });
    int totalPrice = 0;
    checkOut.mealLiistForeach([&](MealStruct& mealItem){
        totalPrice += mealItem.getTotalPrice();
    });

    addFullFont('-');
    addRightFont("總金額: " + QString::number(totalPrice));
    //addRightFont("支付: " + QString::number(checkOut.payMoney));
    //addRightFont("找零: " + QString::number(checkOut.payMoney - totalPrice));
    addRightFont("電話: " + _config["data_storePhone"].toString());

    m_epsonPrinter
            .c_cutPaper(false,1,20)
            .sendBufferToPrinter(false, false);
    m_epsonPrinter.sendBufferToPrinter(true, true);
}

ushort CEpsonPrinter::countOfWordLength(QString text) {
    uint en = 0,ch = 0;
    const QVariantMap& _config = m_config->getRawMap();

    for(const auto &_c : text) { //c++11 support
        if(0x4E00 <= _c && _c <= 0x9FA5)
            ch++;
        else
            en++;
    }

    return (en * _config["epson_englishBytes_dot"].toFloat() + ch * _config["epson_chineseBytes_dot"].toFloat());
}

ushort CEpsonPrinter::countOfWordDot(QString text) {
    uint en = 0,ch = 0;
    for(const auto &_c : text) { //c++11 support
        if(0x4E00 <= _c && _c <= 0x9FA5)
            ch++;
        else
            en++;
    }

    return (en + ch * 2);
}

ushort CEpsonPrinter::centerSpacing(ushort wordLength,uchar sz) {
    const QVariantMap& _config = m_config->getRawMap();
    return (_config["epson_printWidth"].toFloat() - wordLength * (sz + 1)) / 2;
}

ushort CEpsonPrinter::rightSpacing(ushort wordLength,uchar sz) {
    const QVariantMap& _config = m_config->getRawMap();
    return _config["epson_printWidth"].toFloat() - wordLength * (sz + 1);
}

QByteArray CEpsonPrinter::oneLineRemainSpacing(ushort wordLength ,uchar sz) {
    const QVariantMap& _config = m_config->getRawMap();
    QByteArray space = "";
    uint spaceCount = ((ushort)_config["epson_printWidth"].toFloat() - (wordLength * (sz + 1))) / (_config["epson_englishBytes_dot"].toFloat() * (sz + 1));
    for(ushort i = 0; i < spaceCount; i++)
        space += " ";
    return space;
}

QByteArray CEpsonPrinter::makeSpacing(uint count) {
    QByteArray result = "";
    for(uint i = 0; i < count; i++)
        result += " ";
    return result;
}

void CEpsonPrinter::addLeftFont(QString text,uchar sz) {
    m_epsonPrinter
            .c_objLayout('l')
            .c_beginFontSize(sz)
            .c_addFont(text.toUtf8())
            .c_endFontSize();
}

void CEpsonPrinter::addCenterFont(QString text,uchar sz) {
    ushort length = countOfWordLength(text);
    ushort leftSpacingDot = centerSpacing(length , sz);
    m_epsonPrinter
            .c_leftSpacing(leftSpacingDot)
            .c_beginFontSize(sz)
            .c_addFont(text.toUtf8())
            .c_endFontSize();
}

void CEpsonPrinter::addRightFont(QString text,uchar sz) {
    ushort length = countOfWordLength(text);
    ushort leftSpacingDot = rightSpacing(length , sz);
    m_epsonPrinter
            .c_leftSpacing(leftSpacingDot)
            .c_beginFontSize(sz)
            .c_addFont(text.toUtf8())
            .c_endFontSize();
}

void CEpsonPrinter::addSpaceInFontMid(QString leftText,QString rightText,uchar sz) {
    ushort length = countOfWordLength(leftText + rightText);
    QByteArray spaceText = oneLineRemainSpacing(length,sz);
    m_epsonPrinter
            .c_beginFontSize(sz)
            .c_addFont((leftText + spaceText + rightText).toUtf8())
            .c_endFontSize();
}

void CEpsonPrinter::addThreeList(QString leftText,QString midText,QString rightText,uchar leftWidth_dot,uchar midWidth_dot,uchar rightWidth_dot) {
    ushort leftLength = countOfWordDot(leftText);
    ushort midLength = countOfWordDot(midText);
    ushort rightLength = countOfWordDot(rightText);

    if(leftLength > leftWidth_dot || midLength > midWidth_dot || rightLength > rightWidth_dot)
        return;
    leftWidth_dot -= leftLength;
    midWidth_dot -= midLength;
    rightWidth_dot -=  rightLength;


    QString totalText = "";
    totalText += leftText;
    totalText += makeSpacing(leftWidth_dot + midWidth_dot);
    totalText += midText;
    totalText += makeSpacing(rightWidth_dot);
    totalText += rightText;

    m_epsonPrinter
            .c_beginFontSize(0)
            .c_addFont(totalText.toUtf8())
            .c_endFontSize();
}


void CEpsonPrinter::addFullFont(QChar _c,uchar sz) {
    const QVariantMap& _config = m_config->getRawMap();
    float printWidth = _config["epson_printWidth"].toFloat();
    float enWidth = _config["epson_englishBytes_dot"].toFloat();
    uint count = (uint)(printWidth / enWidth);
    QByteArray str = "";
    for(uint i = 0; i < count; i++)
        str += _c;
    m_epsonPrinter
            .c_beginFontSize(sz)
            .c_addFont(str)
            .c_endFontSize();
}
void CEpsonPrinter::addLine(ushort count) {
    m_epsonPrinter.c_lineSpace_b(count);
}

void CEpsonPrinter::openDrawer() {
    m_epsonPrinter.sendBufferToPrinter(true,true);
}
