#include "cerrordebug.h"

CErrorDebug::CErrorDebug(bool isSuccess,QString errorStr,QString errorCode)
{
    m_isSuccess = isSuccess;
    m_errorCode = errorCode;
    m_errorStr = errorStr;
}

CErrorDebug::CErrorDebug(const CErrorDebug& _error)
{

    this->m_isSuccess = _error.m_isSuccess;
    this->m_errorStr = _error.m_errorStr;
    this->m_errorCode = _error.m_errorCode;
}

CErrorDebug& CErrorDebug::operator=(const CErrorDebug &_error) {
    this->m_isSuccess = _error.m_isSuccess;
    this->m_errorStr = _error.m_errorStr;
    this->m_errorCode = _error.m_errorCode;
    return *this;
}

QString CErrorDebug::getLastErrorStr() {
    return m_errorStr;
}

QString CErrorDebug::getLastErrorCode() {
    return m_errorCode;
}

bool CErrorDebug::isSuccess() {
    return m_isSuccess;
}
