#ifndef CERRORDEBUG_H
#define CERRORDEBUG_H

#include <QObject>
#include <QString>

class CErrorDebug
{
public:
    CErrorDebug(bool isSuccess = false,QString errorStr = "",QString errorCode="");
    CErrorDebug(const CErrorDebug&);
    CErrorDebug& operator=(const CErrorDebug &_error);
    Q_INVOKABLE QString getLastErrorStr();
    Q_INVOKABLE QString getLastErrorCode();
    Q_INVOKABLE bool isSuccess();
private:
    QString m_errorStr{""};
    QString m_errorCode{""};
    bool m_isSuccess{false};
};

#endif // CERRORDEBUG_H
