#ifndef CTscPrinterApi_H
#define CTscPrinterApi_H

#include <QObject>
#include <windows.h>
#include <QDebug>
#include "lib/CErrorDebug/cerrordebug.h"

typedef int(__stdcall *TSC_ABOUT_FUNC)();
typedef int(__stdcall *TSC_OPENPORT_FUNC)(const char*);
typedef int(__stdcall *TSC_BARCODE_FUNC)(const char*,const char*, const char*,const char*,const char*,const char*,const char*,const char*,const char*);
typedef int(__stdcall *TSC_CLEARBUFFER_FUNC)();
typedef int(__stdcall *TSC_CLOSEPORT_FUNC)();
typedef int(__stdcall *TSC_DOWNLOADPCX_FUNC)(const char*,const char*);
typedef int(__stdcall *TSC_FORMFEED_FUNC)();
typedef int(__stdcall *TSC_NOBACKFEED_FUNC)();
typedef int(__stdcall *TSC_PRINTERFONT_FUNC)(const char*,const char*,const char*,const char*,const char*,const char*,const char*);
typedef int(__stdcall *TSC_PRINTLABEL_FUNC)(const char*,const char*);
typedef int(__stdcall *TSC_SENDCOMMAND_FUNC)(const char*);
typedef int(__stdcall *TSC_SETUP_FUNC)(const char*,const char*,const char*, const char*,const char*,const char*,const char*);
typedef int(__stdcall *TSC_WINDOWSFONT_FUNC)(int,int,int,int,int,int,const char*,const char*);


class CTscPrinterApi : public QObject
{
    Q_OBJECT
public:
    explicit CTscPrinterApi(QObject *parent = nullptr);
    CErrorDebug initFuncPointer(QString dllPath = "");
    CErrorDebug about();
    CErrorDebug openport(const char*);
    CErrorDebug barcode(const char*,const char*, const char*,const char*,const char*,const char*,const char*,const char*,const char*);
    CErrorDebug clearbuffer();
    CErrorDebug closeport();
    CErrorDebug downloadpcx(const char*,const char*);
    CErrorDebug formfeed();
    CErrorDebug nobackfeed();
    CErrorDebug printerfont(const char*,const char*,const char*,const char*,const char*,const char*,const char*);
    CErrorDebug printlabel(const char*,const char*);
    CErrorDebug sendcommand(const char*);
    CErrorDebug setup(const char*,const char*,const char*, const char*,const char*,const char*,const char*);
    CErrorDebug windowsfont(int,int,int,int,int,int,const char*,const char*);
signals:

public slots:
private:
    TSC_ABOUT_FUNC m_func_about{nullptr};
    TSC_OPENPORT_FUNC m_func_openport{nullptr};
    TSC_BARCODE_FUNC m_func_barcode{nullptr};
    TSC_CLEARBUFFER_FUNC m_func_clearbuffer{nullptr};
    TSC_CLOSEPORT_FUNC m_func_closeport{nullptr};
    TSC_DOWNLOADPCX_FUNC m_func_downloadpcx{nullptr};
    TSC_FORMFEED_FUNC m_func_formfeed{nullptr};
    TSC_NOBACKFEED_FUNC m_func_nobackfeed{nullptr};
    TSC_PRINTERFONT_FUNC m_func_printerfont{nullptr};
    TSC_PRINTLABEL_FUNC m_func_printlabel{nullptr};
    TSC_SENDCOMMAND_FUNC m_func_sendcommand{nullptr};
    TSC_SETUP_FUNC m_func_setup{nullptr};
    TSC_WINDOWSFONT_FUNC m_func_windowsfont{nullptr};
};

#endif // CTscPrinterApi_H
