#include "CTscPrinterApi.h"
#include <QMessageBox>


CTscPrinterApi::CTscPrinterApi(QObject *parent) : QObject(parent)
{

}

CErrorDebug CTscPrinterApi::initFuncPointer(QString dllPath) {
    HMODULE dllAddr = LoadLibraryA(dllPath.toStdString().c_str());
    if (dllAddr == nullptr)
        return CErrorDebug(false,"load dll fail",QString::number(GetLastError()));


    //about
    m_func_about = (TSC_ABOUT_FUNC)GetProcAddress(dllAddr, "about");
    if (m_func_about == nullptr)
        return CErrorDebug(false,"about func fail",QString::number(GetLastError()));


    //openport
    m_func_openport = (TSC_OPENPORT_FUNC)GetProcAddress(dllAddr, "openport");
    if (m_func_openport == nullptr)
        return CErrorDebug(false,"openport func fail",QString::number(GetLastError()));


    //barcode
    m_func_barcode = (TSC_BARCODE_FUNC)GetProcAddress(dllAddr, "barcode");
    if (m_func_barcode == nullptr)
        return CErrorDebug(false,"barcode func fail",QString::number(GetLastError()));


    //clearbuffer
    m_func_clearbuffer = (TSC_CLEARBUFFER_FUNC)GetProcAddress(dllAddr, "clearbuffer");
    if (m_func_clearbuffer == nullptr)
        return CErrorDebug(false,"clearbuffer func fail",QString::number(GetLastError()));


    //closeport
    m_func_closeport = (TSC_CLOSEPORT_FUNC)GetProcAddress(dllAddr, "closeport");
    if (m_func_closeport == nullptr)
        return CErrorDebug(false,"closeport func fail",QString::number(GetLastError()));


    //downloadpcx
    m_func_downloadpcx = (TSC_DOWNLOADPCX_FUNC)GetProcAddress(dllAddr, "downloadpcx");
    if (m_func_downloadpcx == nullptr)
        return CErrorDebug(false,"downloadpcx func fail",QString::number(GetLastError()));


    //formfeed
    m_func_formfeed = (TSC_FORMFEED_FUNC)GetProcAddress(dllAddr, "formfeed");
    if (m_func_formfeed == nullptr)
        return CErrorDebug(false,"formfeed func fail",QString::number(GetLastError()));


    //nobackfeed
    m_func_nobackfeed = (TSC_NOBACKFEED_FUNC)GetProcAddress(dllAddr, "nobackfeed");
    if (m_func_nobackfeed == nullptr)
        return CErrorDebug(false,"nobackfeed func fail",QString::number(GetLastError()));


    //printerfont
    m_func_printerfont = (TSC_PRINTERFONT_FUNC)GetProcAddress(dllAddr, "printerfont");
    if (m_func_printerfont == nullptr)
        return CErrorDebug(false,"printerfont func fail",QString::number(GetLastError()));


    //printlabel
    m_func_printlabel = (TSC_PRINTLABEL_FUNC)GetProcAddress(dllAddr, "printlabel");
    if (m_func_printlabel == nullptr)
        return CErrorDebug(false,"printlabel func fail",QString::number(GetLastError()));


    //sendcommand
    m_func_sendcommand = (TSC_SENDCOMMAND_FUNC)GetProcAddress(dllAddr, "sendcommand");
    if (m_func_sendcommand == nullptr)
        return CErrorDebug(false,"sendcommand func fail",QString::number(GetLastError()));


    //setup
    m_func_setup = (TSC_SETUP_FUNC)GetProcAddress(dllAddr, "setup");
    if (m_func_setup == nullptr)
        return CErrorDebug(false,"setup func fail",QString::number(GetLastError()));


    //windowsfont
    m_func_windowsfont = (TSC_WINDOWSFONT_FUNC)GetProcAddress(dllAddr, "windowsfont");
    if (m_func_windowsfont == nullptr)
        return CErrorDebug(false,"windowsfont func fail",QString::number(GetLastError()));

    return CErrorDebug(true);
}

CErrorDebug CTscPrinterApi::about() {
    int ret = m_func_about();
    return CErrorDebug(ret == 1,"tsc about error","0");
}

CErrorDebug CTscPrinterApi::openport(const char* a) {
    int ret = m_func_openport(a);
    return CErrorDebug(ret == 1,"tsc openport error","0");
}

CErrorDebug CTscPrinterApi::barcode(const char* a,const char* b, const char* c,const char* d,const char* e,const char* f,const char* g,const char* h,const char* i) {
    int ret = m_func_barcode(a,b,c,d,e,f,g,h,i);
    return CErrorDebug(ret == 1,"tsc barcode error","0");
}

CErrorDebug CTscPrinterApi::clearbuffer() {
    int ret = m_func_clearbuffer();
    return CErrorDebug(ret == 1,"tsc clearbuffer error","0");
}

CErrorDebug CTscPrinterApi::closeport() {
    int ret = m_func_closeport();
    return CErrorDebug(ret == 1,"tsc closeport error","0");
}

CErrorDebug CTscPrinterApi::downloadpcx(const char* a,const char* b) {
    int ret = m_func_downloadpcx(a,b);
    return CErrorDebug(ret == 1,"tsc downloadpcx error","0");
}

CErrorDebug CTscPrinterApi::formfeed() {
    int ret = m_func_formfeed();
    return CErrorDebug(ret == 1,"tsc formfeed error","0");
}

CErrorDebug CTscPrinterApi::nobackfeed() {
    int ret = m_func_nobackfeed();
    return CErrorDebug(ret == 1,"tsc nobackfeed error","0");
}

CErrorDebug CTscPrinterApi::printerfont(const char* a,const char* b,const char* c,const char* d,const char* e,const char* f,const char* g) {
    int ret = m_func_printerfont(a,b,c,d,e,f,g);
    return CErrorDebug(ret == 1,"tsc printerfont error","0");
}

CErrorDebug CTscPrinterApi::printlabel(const char* a,const char* b) {
    int ret = m_func_printlabel(a,b);
    return CErrorDebug(ret == 1,"tsc printlabel error","0");
}

CErrorDebug CTscPrinterApi::sendcommand(const char* a) {
    int ret = m_func_sendcommand(a);
    return CErrorDebug(ret == 1,"tsc sendcommand error","0");
}

CErrorDebug CTscPrinterApi::setup(const char* a,const char* b,const char* c, const char* d,const char* e,const char* f,const char* g) {
    int ret = m_func_setup(a,b,c,d,e,f,g);
    return CErrorDebug(ret == 1,"tsc setup error","0");
}

CErrorDebug CTscPrinterApi::windowsfont(int a,int b,int c,int d,int e,int f,const char* g,const char* h) {
    int ret = m_func_windowsfont(a,b,c,d,e,f,g,h);
    return CErrorDebug(ret == 1,"tsc windowsfont error","0");
}
