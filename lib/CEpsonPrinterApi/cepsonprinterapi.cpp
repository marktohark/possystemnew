#include "cepsonprinterapi.h"
#include <QMessageBox>

CEpsonPrinterApi::CEpsonPrinterApi(QObject *parent) : QObject(parent)
{
    m_script = new QByteArray();
    *m_script = "";
    m_serialPort.setBaudRate(QSerialPort::Baud9600);
    m_serialPort.setDataBits(QSerialPort::Data8);
    m_serialPort.setStopBits(QSerialPort::OneStop);
    m_serialPort.setParity(QSerialPort::NoParity);
    m_fontCode = QTextCodec::codecForName("big5");
}

CEpsonPrinterApi::~CEpsonPrinterApi() {
    delete m_script;
    m_serialPort.close();
}

CEpsonPrinterApi& CEpsonPrinterApi::c_addFont(QByteArray text) {
    (*m_script) += m_fontCode->fromUnicode(text + '\n');
    return (*this);
}

CEpsonPrinterApi& CEpsonPrinterApi::c_clearBuffer() {
    (*m_script).clear();
    return (*this);
}

CEpsonPrinterApi& CEpsonPrinterApi::c_beginFontSize(uchar sz) {
    c_beginFontSize(sz,sz);
    return (*this);
}

CEpsonPrinterApi& CEpsonPrinterApi::c_beginFontSize(uchar width,uchar height) {
    if(width > 7 || height > 7) {
        qDebug() << "font size can't big than 7";
        return (*this);
    }
    (*m_script) += QByteArray::fromRawData("\x1d\x21",2);
    width <<= 4;
    char _byte = (width | height);
    *m_script += QByteArray::fromRawData(&_byte,1);
    return (*this);
}

CEpsonPrinterApi& CEpsonPrinterApi::c_endFontSize() {
    c_beginFontSize(0);
    return (*this);
}

QByteArray CEpsonPrinterApi::getCurBufferContent() {
    return *m_script;
}

CEpsonPrinterApi& CEpsonPrinterApi::c_cutPaper(bool fullcut,uchar mode,uchar pos) {
    char _c = 0;
    *m_script += QByteArray::fromRawData("\x1d\x56",2);
    switch(mode) {

    case 0: { //func A
        _c = (fullcut?'0':'1');
        *m_script += QByteArray::fromRawData(&_c,1);
        return (*this);
    }

    case 1:{ //func B
        _c = (char)(fullcut?65:66);
        break;
    }

    case 2:{ //func C
        _c = (char)(fullcut?97:98);
        break;
    }

    case 3:{ //func D
        _c = (char)(fullcut?103:104);
        break;
    }
    default:
        return (*this);
    }
    *m_script += QByteArray::fromRawData(&_c,1);
    *m_script += QByteArray::fromRawData((char*)&pos,1);
    return (*this);
}

CEpsonPrinterApi& CEpsonPrinterApi::c_lineSpace(uchar count) {
    *m_script += QByteArray::fromRawData("\x1B\x33",2);
    *m_script += QByteArray::fromRawData((char*)&count,1);
    return (*this);
}

CEpsonPrinterApi& CEpsonPrinterApi::c_objLayout(uchar layout) {
    char _c = (layout=='l'?'0':
                           (layout=='r'?'2':
                                        (layout=='c'?'1':'3')));
    if(_c == '3')
        return (*this);
    *m_script += QByteArray::fromRawData("\x1B\x61",2);
    *m_script += QByteArray::fromRawData(&_c,1);
    return (*this);
}
CEpsonPrinterApi& CEpsonPrinterApi::c_init() {
    *m_script += QByteArray::fromRawData("\x1B\x40",2);
    return (*this);
}

CEpsonPrinterApi& CEpsonPrinterApi::c_initLineSpace() {
    *m_script += QByteArray::fromRawData("\x1B\x32",2);
    return (*this);
}

CEpsonPrinterApi& CEpsonPrinterApi::c_feedLine(uchar count) {
    *m_script += QByteArray::fromRawData("\x1B\x4A",2);
    *m_script += QByteArray::fromRawData((char*)&count,1);
    return (*this);
}

CEpsonPrinterApi& CEpsonPrinterApi::c_lineSpace_b(uchar count) {
    *m_script += QByteArray::fromRawData("\x1B\x64",2);
    *m_script += QByteArray::fromRawData((char*)&count,1);
    return (*this);
}

CEpsonPrinterApi& CEpsonPrinterApi::c_sendPlus(uchar pin,uchar plusT,uchar spaceT) {
    if(pin != 1 && pin != 0)
        return (*this);

    *m_script += QByteArray::fromRawData("\x1B\x70",2);
    *m_script += QByteArray::fromRawData((char*)&pin,1);
    *m_script += QByteArray::fromRawData((char*)&plusT,1);
    *m_script += QByteArray::fromRawData((char*)&spaceT,1);
    return (*this);
}

CEpsonPrinterApi& CEpsonPrinterApi::c_setPrintX(ushort x) {
    *m_script += QByteArray::fromRawData("\x1d\x4c",2);
    char _l = x % 256 , _h = x / 256;
    *m_script += QByteArray::fromRawData((char*)&_l,1);
    *m_script += QByteArray::fromRawData((char*)&_h,1);
    return (*this);
}

CEpsonPrinterApi& CEpsonPrinterApi::c_setPrintWidth(ushort width) {
    *m_script += QByteArray::fromRawData("\x1d\x57",2);
    char _l = width % 256 , _h = width / 256;
    *m_script += QByteArray::fromRawData((char*)&_l,1);
    *m_script += QByteArray::fromRawData((char*)&_h,1);
    return (*this);
}

CEpsonPrinterApi& CEpsonPrinterApi::c_leftSpacing(ushort dot) {
    *m_script += QByteArray::fromRawData("\x1B\x24",2);
    char _l = dot % 256 , _h = dot / 256;
    *m_script += QByteArray::fromRawData((char*)&_l,1);
    *m_script += QByteArray::fromRawData((char*)&_h,1);
    return (*this);
}

uint CEpsonPrinterApi::getDataSize() {
    return m_script->length();
}

QSerialPort& CEpsonPrinterApi::getSerialPort() {
    return m_serialPort;
}

void CEpsonPrinterApi::setPortName(const QString& name) {
    m_serialPort.setPortName(name);
}

CErrorDebug CEpsonPrinterApi::openSerialPort(QSerialPort::OpenMode mode) {
    if(!m_serialPort.open(mode))
        return CErrorDebug(false,m_serialPort.errorString(),QString::number((int)m_serialPort.error()));
    return CErrorDebug(true);
}

uint CEpsonPrinterApi::sendBufferToPrinter(bool clearBufr,bool openDraw) {
    if(openDraw)
        this->c_sendPlus(0,10,20);
    uint sendSz = m_serialPort.write(*m_script);
    if(clearBufr)
        m_script->clear();
    return sendSz;
}
