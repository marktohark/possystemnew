#include "checkoutstruct.h"

CheckOutStruct::CheckOutStruct(int _number,
               int _payMoney,
               int _totalPrice,
               ENUM_TRANSFER _takeOut,
               QList<MealStruct> _mealList)
    : number(_number) ,
      payMoney(_payMoney),
      totalPrice(_totalPrice),
      takeOut(_takeOut),
      mealList(_mealList) {

}

QString CheckOutStruct::getCurTransferStr() { //enum 2 str
    QString result = "未知";
    switch (takeOut) {
    case enum_takeOut:
        result = "外帶";
        break;
    case enum_eatHere:
        result = "內用";
        break;
    case enum_takeSelf:
        result = "自取";
        break;
    case enum_order:
        result = "外送";
        break;
    default:
        break;
    }
    return result;
}

void CheckOutStruct::mealLiistForeach(std::function<void(MealStruct&)> func) {
    for(MealStruct& item : mealList)
        func(item);
}

void CheckOutStruct::clear() {
    mealList.clear();
    payMoney = 0;
    number = 0;
    totalPrice = 0;
    takeOut = enum_eatHere;
}

void CheckOutStruct::calcMealTotalPrice() {
    int _totalPrice = 0;
    this->mealLiistForeach([&](auto& mealItem){
        _totalPrice += mealItem.getTotalPrice();
    });
    this->totalPrice = _totalPrice;
}
