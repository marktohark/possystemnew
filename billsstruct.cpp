#include "billsstruct.h"
#include <QDebug>
BillsStruct::BillsStruct(int count)
{
    for(int i = 0; i < count; i++)
        m_checkoutList[i] = CheckOutStruct();
    m_curIter = m_checkoutList.find(0);
}

void BillsStruct::setCheckOutStruct(int index,const CheckOutStruct* check) {
    m_curIter = m_checkoutList.find(index);
    if(m_curIter == m_checkoutList.end()) {
        m_checkoutList[index] = CheckOutStruct();
        m_curIter = m_checkoutList.find(index);
    }
    else
        if(check != nullptr)
            (*m_curIter) = *check;
    return;
}

CheckOutStruct &BillsStruct::getCheckOutStruct() {
    auto& Struct  = (*m_curIter);

    return Struct;
}

int BillsStruct::count() {
    return m_checkoutList.count();
}

bool BillsStruct::isEmpty(int index) {
    auto iter = m_checkoutList.find(index);
    return (*iter).mealList.count() <= 0;
}
