#ifndef MEALINFO_H
#define MEALINFO_H
#include <QVariantMap>

class MealInfo {
public:
    MealInfo(int __size = enum_non, int _price = -1);
    MealInfo(QVariantMap data);

    QVariantMap toMap();// {size:??,sizeStr:??,price:??}

    void byMap(QVariantMap data);

    QString getSizeStr(); //根據size數值判斷str輸出

public:
    enum ENUM_SIZE {
        enum_non = -1,
        enum_small,
        enum_mid,
        enum_big,
        enum_end
    };
    ENUM_SIZE size{enum_non};
    QString sizeStr{""};
    int price{-1};
};
#endif // MEALINFO_H
