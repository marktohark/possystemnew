#ifndef DISCOUNTSTRUCT_H
#define DISCOUNTSTRUCT_H

#include "mealstruct.h"

class MealStruct;
class DiscountStruct {
public:
    int getPrice(MealStruct& meal);

public:
    enum ENUM_DISCOUNT {
        enum_non = -1,
        enum_free,      //贈
        enum_discount,  //折
        enum_chargeBack,//扣
        enum_end
    };
    int x{-1}; //各種模式的變數
    ENUM_DISCOUNT mode{enum_non};
};

#endif // DISCOUNTSTRUCT_H
